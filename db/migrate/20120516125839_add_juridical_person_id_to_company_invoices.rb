class AddJuridicalPersonIdToCompanyInvoices < ActiveRecord::Migration
  def change
    add_column :company_invoices, :juridical_person_id, :integer
    add_index :company_invoices, :juridical_person_id
  end
end
