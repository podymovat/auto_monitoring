module Web::Company::AccountsHelper
  def link_to_edit_account_by_client(account)
    if account.type == ::Company::CrmAccount.name
      edit_company_crm_account_path(account)
    else account.type == ::Company::GisAccount.name
      edit_company_gis_account_path(account)
    end
  end
end
