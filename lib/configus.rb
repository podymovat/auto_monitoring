# encoding: utf-8

Configus.build Rails.env do
  env :production do
    kaize 'http://kaize.ru'

    phone do
      defaults do
        area_code '495' # Moscow
      end
      country_codes [7, 8] # Russia
    end

    pagination do
      per_page_values [5, 10, 20, 50]
    end
  end

  env :staging, :parent => :production do
    basic_auth do
      username 'admin'
      password '54321'
    end
  end

  env :development, :parent => :production do
      username 'admin'
      password '54321'
  end

  env :test, :parent => :development
end
