class AddUniqueIndexOnNameToCompanyCounteragents < ActiveRecord::Migration
  def change
    add_index :company_counteragents, [:name, :company_id, :kind_id], :unique => true
  end
end
