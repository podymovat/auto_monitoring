class CreateJuridicalPeople < ActiveRecord::Migration
  def change
    create_table :juridical_people do |t|
      t.string :name
      t.string :invoice_number_prefix
      t.string :state

      t.timestamps
    end
  end
end
