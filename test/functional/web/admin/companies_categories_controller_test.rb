require 'test_helper'

class Web::Admin::CompaniesCategoriesControllerTest < ActionController::TestCase
  def setup
    @signed_in_employee = create(:employee)
    sign_in(@signed_in_employee)
    @company_category = create('company/category')
    @params = {:id => @company_category.id}
  end

  test "should get index" do
    get :index
    assert_response :success
  end

  test "should get new" do
    get :new
    assert_response :success
  end

  test "should post create" do
    attrs = attributes_for 'company/category'

    post :create, :company_category => attrs
    assert_response :redirect

    category = ::Company::Category.last
    assert_equal attrs[:name], category.name
  end

  test "should get edit" do
    get :edit, @params
    assert_response :success
  end

  test "should put update" do
    attrs = {}
    attrs[:name] = generate :name
    put :update, @params.merge(:company_category => attrs)
    assert_response :redirect

    @company_category.reload
    assert_equal attrs[:name], @company_category.name
  end

  test "should delete destroy" do
    delete :destroy, @params
    assert_response :redirect

    assert !::Company::Category.exists?(@params[:id])
  end
end
