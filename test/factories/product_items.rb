FactoryGirl.define do
  factory :product_item do
    creator
    name
    prime_amount
    sail_amount
  end
end
