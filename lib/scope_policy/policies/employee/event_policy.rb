class ScopePolicy::Policies::Employee::EventPolicy < ScopePolicy::Policies::Employee::BasePolicy

  def model_relation
    @relation = ::Employee::Event.scoped
  end

  def events_list
    if !@current_user.can?(:company_manage_all) || @options[:show_assigned_to_me]
      @relation = @relation.company_assigned_to(@current_user)
    end
    @relation
  end
end
