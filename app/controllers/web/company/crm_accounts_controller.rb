class Web::Company::CrmAccountsController < Web::Company::ProtectedApplicationController
  add_i18n_breadcrumb :general, :company_accounts_path
  def edit
    @crm_account = current_company.crm_accounts.find(params[:id])

    add_breadcrumb @crm_account, :edit_company_crm_account_path
  end

  def update
    @crm_account = current_company.crm_accounts.find(params[:id])

    if @crm_account.update_attributes(params[:company_crm_account])
      flash[:success] = flash_translate(:success)
      redirect_to company_accounts_path
    else
      add_breadcrumb @crm_account.login, :edit_company_crm_account_path
      render :action => :edit
    end
  end
end
