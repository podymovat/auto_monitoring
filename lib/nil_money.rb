# Public: Fake Money class. Use it when you need to allow nil values.
class NilMoney

  # Public: Initialize a NilMoney.
  #
  # name - Original value (default: '').
  def initialize(value = '')
    @value = value
  end

  def cents
    nil
  end

  def currency_as_string
    nil
  end

  def nil?
    true
  end

  def to_s
    @value
  end
end
