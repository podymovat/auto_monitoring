module Company::PersonRepository
  extend ActiveSupport::Concern
  include UsefullScopes

  included do
    scope :ilike_by_contact_details_entry, lambda { |value|
      joins(:contact_details)
        .where('name ILIKE :value OR
                contact_details.kind_value ILIKE :value',
               :value => "%#{value}%")
        .group('company_people.id')
    }
    scope :company_assigned_to, lambda { |value| joins(:company).where(:companies => {:assigned_id => value})}
  end
end
