class AddUniqueIndexOnNameToCompanyCategories < ActiveRecord::Migration
  def change
    add_index :company_categories, :name, :unique => true
  end
end
