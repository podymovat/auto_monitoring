# encoding: utf-8

Допустим /^"(.*?)" является работником$/ do |first_name|
  Employee.create(first_name: first_name, last_name: 'Иванов', email: 'test@test.ru', password: '54321')
end

Если /^он заходит на страницу авторизации работника$/ do
  visit new_employee_session_path
end

Если /^вводит в поле "([^"]*)" свой email$/ do |email|
  fill_in(email, :with => 'test@test.ru')
end

Если /^вводит в поле "([^"]*)" свой пароль$/ do |password|
  fill_in(password, :with => '54321')
end

Если /^нажимает "(.*)"$/ do |button|
  click_button(button)
end

Тогда /^он должен быть на странице списка компаний$/ do
  current_path == employee_companies_path
end