class Web::Employee::SessionsController < Web::Employee::ApplicationController
  def new
  end

  def create
    employee = ::Employee.find_by_email(params[:employee][:email])
    if employee && employee.authenticate(params[:employee][:password])
      reset_session
      sign_in(employee)
      redirect_to :employee_companies
    else
      flash[:error] = t('authorization.errors.incorrect')
      render :action => :new
    end
  end

  def destroy
    employee_sign_out
    redirect_to :employee_companies
  end
end
