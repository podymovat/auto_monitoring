class RenameJuridicalPersonIdToCounteragentIdOnCompanyDeals < ActiveRecord::Migration
  def change
    rename_column :company_deals, :juridical_person_id, :counteragent_id
    rename_index :company_deals, :index_company_deals_on_juridical_person_id,
      :index_company_deals_on_counteragent_id
  end
end
