class Web::Admin::EmployeesController < Web::Admin::ProtectedApplicationController
  add_i18n_breadcrumb :index, :admin_employees_path

  def index
    title t(:employees)

    params[:search] = {:meta_sort => 'id.asc'}.merge(params[:search] || {})
    @search = ::Employee.metasearch(params[:search])
    @employees = @search.page(params[:page]).per(params[:per_page])
  end

  def new
    @employee = NewEmployeeType.new

    add_i18n_breadcrumb :new, :new_admin_employee_path
  end

  def show
    @employee = ::Employee.find(params[:id])

    add_breadcrumb @employee, :admin_employee_path
  end

  def edit
    @employee = ::Employee.find(params[:id])

    add_breadcrumb @employee, :edit_admin_employee_path
  end

  def create
    @employee = NewEmployeeType.new(params[:employee])

    if @employee.save
      flash[:success] = flash_translate(:success)
      redirect_to :action => :index
    else
      add_i18n_breadcrumb :new, :new_admin_employee_path
      render :action => :new
    end
  end

  def update
    @employee = ::Employee.find(params[:id])

    add_breadcrumb @employee.full_name, :edit_admin_employee_path

    if @employee.update_attributes(params[:employee])
      flash[:success] = flash_translate(:success)
      redirect_to :action => :index
    else
      render :action => :edit
    end
  end

  def trigger_state_event
    @employee = ::Employee.find(params[:id])
    @employee.fire_state_event(params[:event])

    redirect_to :action => :index
  end
end
