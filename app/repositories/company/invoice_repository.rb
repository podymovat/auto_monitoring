module Company::InvoiceRepository
  extend ActiveSupport::Concern
  include UsefullScopes

  included do
    scope :ilike_by_invoice_number_entry, lambda { |value|
      where('invoice_number ILIKE :value',
            :value => "%#{value}%")
    }

    scope :active, where(:state => :active)
    scope :company_assigned_to, lambda { |value| joins(:company).where(:companies => {:assigned_id => value})}
  end
end
