class Web::Employee::CompaniesCrmAccountsController < Web::Employee::ProtectedApplicationController
  add_i18n_breadcrumb :general, :employee_companies_path
  def new
    @company_crm_account = NewCompanyCrmAccountType.new(params[:company_crm_account])

    add_i18n_breadcrumb :index, :employee_companies_accounts_path
    add_i18n_breadcrumb :new, :new_employee_companies_crm_account_path
  end

  def create
    @company_crm_account = NewCompanyCrmAccountType.new(params[:company_crm_account])
    @company_crm_account.creator = current_employee

    if @company_crm_account.save
      flash[:success] = flash_translate(:success)
      redirect_to employee_company_path(@company_crm_account.company)
    else
      add_i18n_breadcrumb :index, :employee_companies_accounts_path
      add_i18n_breadcrumb :new, :new_employee_companies_crm_account_path
      render :action => :new
    end
  end

  def edit
    scope = build_scope 'company/account'
    @company_crm_account = scope.get(params[:id])

    add_breadcrumb @company_crm_account.company, employee_company_path(@company_crm_account.company)
    add_i18n_breadcrumb :index, :employee_companies_accounts_path
    add_breadcrumb @company_crm_account, :edit_employee_companies_crm_account_path
  end

  def update
    scope = build_scope 'company/account'
    @company_crm_account = scope.get(params[:id])

    if @company_crm_account.update_attributes(params[:company_crm_account])
      flash[:success] = flash_translate(:success)
      redirect_to employee_company_path(@company_crm_account.company)
    else
      add_breadcrumb @company_crm_account.company, employee_company_path(@company_crm_account.company)
      add_i18n_breadcrumb :index, :employee_companies_accounts_path
      add_breadcrumb @company_crm_account.login, :edit_employee_companies_crm_account_path
      render :action => :edit
    end
  end
end
