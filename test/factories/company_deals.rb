FactoryGirl.define do
  factory 'company/deal', :aliases => [:company_deal] do
    company
    deal_number
    deal_date
    total_amount
    paid_amount
    creator
    association :counteragent, :factory => 'company/counteragent'
    juridical_person
  end
end
