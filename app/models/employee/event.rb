class Employee::Event < ActiveRecord::Base
  include EventRepository

  attr_accessible :description, :due_date, :company_id, :company_person_id

  belongs_to :creator, :class_name => ::Employee, :foreign_key => 'employee_id'
  belongs_to :company_person, :class_name => ::Company::Person, :foreign_key => 'company_person_id'
  belongs_to :company

  validates :creator, :presence => true
  validates :company, :presence => true
  validates :description, :presence => true, :length => {:maximum => 100}
  validates :due_date, :presence => true

  state_machine :state, :initial => :new do
    state :new
  end

  def description_short
    description.truncate(40)
  end
end
