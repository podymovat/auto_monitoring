class CreateCompanyCounteragents < ActiveRecord::Migration
  def change
    create_table :company_counteragents do |t|
      t.string :name
      t.integer :kind_id
      t.integer :company_id
      t.string :state

      t.timestamps
    end
  end
end
