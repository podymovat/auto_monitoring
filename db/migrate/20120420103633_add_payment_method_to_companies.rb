class AddPaymentMethodToCompanies < ActiveRecord::Migration
  def change
    change_table :companies do |t|
      t.remove :payment_method
      t.references :payment_method
    end
  end
end
