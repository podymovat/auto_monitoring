FactoryGirl.define do
  factory 'employee/position' do
    name
    association :department, :factory => 'employee/department'
  end
end
