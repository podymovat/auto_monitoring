class AddUniqueIndexOnLoginToCompanyAccounts < ActiveRecord::Migration
  def change
    add_index :company_accounts, :login, :unique => true
  end
end
