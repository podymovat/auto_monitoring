class DropCompanyDealKinds < ActiveRecord::Migration
  def up
    drop_table :company_deal_kinds
  end

  def down
    create_table :company_deal_kinds do |t|
      t.string :name
    end
  end
end
