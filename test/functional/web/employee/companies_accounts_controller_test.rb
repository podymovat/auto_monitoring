require 'test_helper'

class Web::Employee::CompaniesAccountsControllerTest < ActionController::TestCase
  def setup
    @employee = create(:employee)
    sign_in(@employee)
    @company = create(:company, :creator => @employee, :assigned => @employee)
    @company_person = create('company/person', :creator => @employee, :company => @company)
    @company_crm_account = create('company/crm_account', :company => @company, :person => @company_person)
    @company_gis_account = create('company/gis_account', :company => @company)
    @params = {:id => @company_crm_account.id}
  end

  test "should get index" do
    get :index
    assert_response :success
  end

  test "should delete destroy" do
    delete :destroy, @params
    assert_response :redirect

    assert !::Company::Account.exists?(@params[:id])
  end
end
