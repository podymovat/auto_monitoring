class Api::PhonesController < Api::ApplicationController

  def exists
    if params[:q]
      clean_value = Filters.phone(params[:q])
      result = ContactDetail.phones
      result = result.exclude(params[:id]) if params[:id]
      result = result.find_by_kind_value(clean_value)

      respond_with({ exists: result.present?, error: ErrorMessages.for_contact_detail(result, view_context) })
    end
  end
end
