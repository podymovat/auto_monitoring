class Company::Person < ActiveRecord::Base
  include PersonRepository

  attr_accessible :company_id, :name, :position, :contact_details_attributes, :state_event

  belongs_to :company
  belongs_to :creator, :class_name => ::Employee, :foreign_key => 'employee_id'

  has_many :contact_details, :as => :contactable
  has_many :notes, :dependent => :destroy
  has_many :employee_events, :class_name => 'Employee::Event', :foreign_key => 'company_person_id'
  has_many :accounts

  validates :company, :presence => true
  validates :name, :length => {:maximum => 255}, :presence => true
  validates :creator, :presence => true
  validates :position, :length => {:maximum => 255}

  accepts_nested_attributes_for :contact_details, :reject_if => :all_blank, :allow_destroy => true

  state_machine :state, :initial => :new do
    state :new
  end

  def to_s
    name
  end
end
