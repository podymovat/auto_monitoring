class AddInvoiceNumberToCompanyInvoices < ActiveRecord::Migration
  def change
    add_column :company_invoices, :invoice_number, :string
  end
end
