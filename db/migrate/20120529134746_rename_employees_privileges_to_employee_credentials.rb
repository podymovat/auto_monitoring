class RenameEmployeesPrivilegesToEmployeeCredentials < ActiveRecord::Migration
  def change
    rename_table :employees_privileges, :employee_credentials
  end
end
