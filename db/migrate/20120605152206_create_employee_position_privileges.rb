class CreateEmployeePositionPrivileges < ActiveRecord::Migration
  def change
    create_table :employee_position_privileges do |t|
      t.references :position
      t.references :privilege

      t.timestamps
    end
  end
end
