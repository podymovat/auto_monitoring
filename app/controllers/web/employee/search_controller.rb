class Web::Employee::SearchController < Web::Employee::ProtectedApplicationController

  def index
    @query = params[:term] ? params[:term].strip : ''
    @companies = @company_people = @company_invoices = []
    unless @query.empty?
      @companies = ::Company.ilike_by_contact_details_entry(@query)
      @company_people = ::Company::Person.ilike_by_contact_details_entry(@query)
      @company_invoices = ::Company::Invoice.ilike_by_invoice_number_entry(@query)
    end
  end
end
