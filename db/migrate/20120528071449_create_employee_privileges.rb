class CreateEmployeePrivileges < ActiveRecord::Migration
  def change
    create_table :employee_privileges do |t|
      t.string :name
      t.string :key
    end
  end
end
