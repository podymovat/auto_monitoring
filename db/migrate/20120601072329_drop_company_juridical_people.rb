class DropCompanyJuridicalPeople < ActiveRecord::Migration
  def up
    drop_table :company_juridical_people
  end

  def down
    create_table :company_juridical_people do |t|
      t.integer :company_id
      t.string :name
    end
  end
end
