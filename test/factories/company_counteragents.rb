FactoryGirl.define do
  factory 'company/counteragent' do
    name
    association :kind, :factory => 'company/counteragent/kind'
    company
    cars_count
  end
end
