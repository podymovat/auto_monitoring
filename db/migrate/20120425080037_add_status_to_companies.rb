class AddStatusToCompanies < ActiveRecord::Migration
  def change
    change_table :companies do |t|
      t.references :company_status
    end
  end
end
