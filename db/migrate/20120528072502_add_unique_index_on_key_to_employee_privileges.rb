class AddUniqueIndexOnKeyToEmployeePrivileges < ActiveRecord::Migration
  def change
    add_index :employee_privileges, :key, :unique => true
  end
end
