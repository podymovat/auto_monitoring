class AddCompanyDealToCompanyInvoice < ActiveRecord::Migration
  def change
    change_table :company_invoices do |t|
      t.references :company_deal
    end
  end
end
