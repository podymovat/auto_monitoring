class Web::Company::ApplicationController < Web::ApplicationController
  layout 'web/company/application'

  before_filter :company_title
  helper_method :company_account_signed_in?, :current_company_account

  private

  def company_title
    title t("title.company")
  end

  def current_company
    current_company_account.company
  end
end
