require 'test_helper'

class Web::Company::SessionsControllerTest < ActionController::TestCase
  test "should authenticate" do
    attrs = {}
    company_crm_account = create('company/crm_account')
    attrs[:login] = company_crm_account.login
    attrs[:password] = company_crm_account.password
    post :create, :company_crm_account => attrs
    assert_response :redirect
    assert company_account_signed_in?
  end

  test "should not authenticate" do
    attrs = {}
    company_crm_account = create('company/crm_account')
    attrs[:login] = company_crm_account.login
    attrs[:password] = 'wrong_pass'
    post :create, :company_crm_account => attrs
    assert_response :success
    assert !company_account_signed_in?
  end

  test "should get new" do
    get :new
    assert_response :success
  end

  test "should delete destroy" do
    company_crm_account = create('company/crm_account')
    sign_in(company_crm_account)
    delete :destroy
    assert_response :redirect
    assert !company_account_signed_in?
  end
end
