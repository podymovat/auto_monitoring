class Api::ProductItemsController < Api::ApplicationController
  def show
    @product_item = ProductItem.find(params[:id])
    respond_with(@product_item)
  end
end

