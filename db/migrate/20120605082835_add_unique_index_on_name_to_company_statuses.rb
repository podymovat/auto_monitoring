class AddUniqueIndexOnNameToCompanyStatuses < ActiveRecord::Migration
  def change
    add_index :company_statuses, :name, :unique => true
  end
end
