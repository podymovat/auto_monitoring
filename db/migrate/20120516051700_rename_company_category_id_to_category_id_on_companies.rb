class RenameCompanyCategoryIdToCategoryIdOnCompanies < ActiveRecord::Migration
  def change
    rename_column :companies, :company_category_id, :category_id
  end
end
