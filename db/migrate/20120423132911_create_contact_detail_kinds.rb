class CreateContactDetailKinds < ActiveRecord::Migration
  def change
    create_table :contact_detail_kinds do |t|
      t.string :name
    end
  end
end
