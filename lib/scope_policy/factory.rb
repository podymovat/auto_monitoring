class ScopePolicy::Factory

  def self.build(model_name, current_user, options = {})
    class_name = "ScopePolicy::Policies::#{model_name.to_s.camelize}Policy"
    class_name.constantize.new(current_user, options)
  end
end
