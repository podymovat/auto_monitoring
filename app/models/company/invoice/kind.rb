class Company::Invoice::Kind < ActiveRecord::Base
  include UsefullScopes

  LICENSE_FEE_ID = 1
  USUAL_ID = 2

  attr_accessible :name

  validates :name, :presence => true, :uniqueness => {:case_sensitive => false},
            :length => {:maximum => 255}

  def to_s
    name
  end

  def license_fee?
    id == LICENSE_FEE_ID
  end
end
