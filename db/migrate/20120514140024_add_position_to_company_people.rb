class AddPositionToCompanyPeople < ActiveRecord::Migration
  def up
    add_column :company_people, :position, :string
  end

  def down
    remove_column :company_people, :position
  end
end
