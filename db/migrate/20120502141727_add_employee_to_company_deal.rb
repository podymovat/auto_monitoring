class AddEmployeeToCompanyDeal < ActiveRecord::Migration
  def change
    change_table :company_deals do |t|
      t.references :employee
    end
  end
end
