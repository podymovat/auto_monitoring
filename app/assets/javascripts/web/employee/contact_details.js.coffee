$ = jQuery
$.fn.extend
  # Sends an ajax request in order to check
  # if the following contact detail already exists
  contact_detail: (options) ->
    settings =
      error_class: 'error'

    settings = $.extend settings, options

    process_contact_detail = (input, url) ->
      params =
        q: input.value

      # check for id (in case it is edit)
      control_group = $(input).closest('.control-group')
      contact_detail_id_input = control_group.next('input[type="hidden"]')
      if (contact_detail_id_input.length > 0)
        params['id'] = contact_detail_id_input.val()

      $.ajax(
        url: url,
        data: params,
        cache: false,
        statusCode:
          401: ->
            window.location.reload()
      ).done((data) ->
        if data.exists
          if !control_group.hasClass(settings.error_class)
            control_group.addClass(settings.error_class)
            $('<span class="help-block">').html(data.error).appendTo(control_group.children())
          else
            $('span.help-block', control_group).html(data.error)
        else
          control_group.removeClass(settings.error_class)
          control_group.children().find('span.help-block').remove()
      )

    return @each ()->
      # search for existing phone
      $('form').on 'change', 'input[type="text"][rel="phone"].contact_detail', ->
        process_contact_detail(this, Routes.exists_api_phones_path())

      # search for existing email
      $('form').on 'change', 'input[type="text"][rel="email"].contact_detail', ->
        process_contact_detail(this, Routes.exists_api_emails_path())

      # changing contact_detail's kind
      $('form').on 'change', 'select.contact_detail_kind', ->
        # remove error on change
        control_group = $(this).closest('.control-group')
        if control_group.hasClass(settings.error_class)
          control_group.removeClass(settings.error_class)
          control_group.find('span.help-block').remove()

        if $(this).val()
          contact_detail_input = $(this).next('input')
          # set up rel attribute based on selected option
          contact_detail_input.attr('rel', $('option:selected', this).attr('rel'))
$ ->
  $(document).contact_detail()