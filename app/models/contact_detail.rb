class ContactDetail < ActiveRecord::Base
  include ContactDetailRepository

  EMAIL_ID = 4

  attr_accessible :kind_id, :kind_value_source

  belongs_to :contactable, :polymorphic => true
  belongs_to :kind

  validates :kind_value_source, :presence => true, :length => {:maximum => 255}

  # custom validation
  validates :kind_value, :contact_detail_uniqueness => {:case_sensitive => false}, :phone => true, :if => :phone?
  validates :kind_value, :contact_detail_uniqueness => {:case_sensitive => false}, :email => true, :if => :email?
  validates :kind_value, :contact_detail_uniqueness => {:case_sensitive => false}, :url => true, :if => :site?

  def to_s
    kind_value_source || kind_value
  end

  def phone?
    kind == ContactDetail::Kind.phone
  end

  def email?
    kind == ContactDetail::Kind.email
  end

  def site?
    kind == ContactDetail::Kind.site
  end

  def kind_value_source=(v)
    value = v
    write_attribute(:kind_value_source, value) # save original value
    if phone?
      value = Filters.phone(v)
    end
    if site?
      value = Filters.site(v)
    end
    self.kind_value = value
  end
end
