require 'test_helper'

class Web::Company::GisAccountsControllerTest < ActionController::TestCase
  def setup
    @employee = create(:employee)
    sign_in(@employee)
    @company = create(:company, :creator => @employee, :assigned => @employee)
    @company_person = create('company/person', :creator => @employee, :company => @company)
    @company_crm_account = create('company/crm_account', :company => @company, :person => @company_person)
    @company_gis_account = create('company/gis_account', :company => @company)
    employee_sign_out

    sign_in(@company_crm_account)
    @params = {:id => @company_gis_account.id}
  end

  test "should get edit" do
    get :edit, @params
    assert_response :success
  end

  test "should put update" do
    attrs = {}
    attrs[:password_text]= generate :password
    attrs[:login] = generate :login
    put :update, @params.merge(:company_gis_account => attrs)
    assert_response :redirect

    @company_gis_account.reload
    assert_equal attrs[:login], @company_gis_account.login
  end
end
