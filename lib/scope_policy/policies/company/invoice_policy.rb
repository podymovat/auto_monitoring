class ScopePolicy::Policies::Company::InvoicePolicy < ScopePolicy::Policies::Company::BasePolicy

  def model_relation
    @relation = ::Company::Invoice.scoped
  end

  def invoices_list
    if !@current_user.can?(:company_manage_all) || @options[:show_assigned_to_me]
      @relation = @relation.company_assigned_to(@current_user)
    end
    unless @current_user.can? :invoice_manage_all
      @relation = @relation.active
    end
    @relation
  end

  def get(id)
    object = @relation.find(id)
    unless EmployeeAccessController.can_access_company_invoice?(@current_user, object)
      raise Employee::Privilege::ObjectAccessDeniedError.new("Access denied for #{object.class.model_name}", object)
    end
    object
  end
end
