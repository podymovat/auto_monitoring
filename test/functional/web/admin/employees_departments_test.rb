require 'test_helper'

class Web::Admin::EmployeesDepartmentsControllerTest < ActionController::TestCase

  def setup
    @signed_in_employee = create(:employee)
    sign_in(@signed_in_employee)
    @employee_department = create('employee/department')
    @params = {:id => @employee_department.id}
  end

  test "should get index" do
    get :index
    assert_response :success
  end

  test "should get new" do
    get :new
    assert_response :success
  end

  test "should get edit" do
    get :edit, @params
    assert_response :success
  end

  test "should post create" do
    attrs = attributes_for 'employee/department'

    post :create, :employee_department => attrs
    assert_response :redirect

    employee_department = ::Employee::Department.last
    assert employee_department
  end

  test "should put update" do
    name = @employee_department.name

    attrs = {}
    attrs[:name] = generate :name
    put :update, @params.merge(:employee_department => attrs)
    assert_response :redirect

    @employee_department.reload
    assert @employee_department.name != name
  end

  test "should delete destroy" do
    delete :destroy, @params
    assert_response :redirect

    assert !Employee::Department.exists?(@employee_department.id)
  end
end
