class RenameLoginToEmailOnEmployees < ActiveRecord::Migration
  def change
    rename_column :employees, :login, :email
  end
end
