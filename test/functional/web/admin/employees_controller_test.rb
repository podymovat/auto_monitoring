require 'test_helper'

class Web::Admin::EmployeesControllerTest < ActionController::TestCase

  def setup
    @signed_in_employee = create(:employee)
    sign_in(@signed_in_employee)
    @employee = create(:employee)
    @employee_privilege = create('employee/privilege')
    @params = {:id => @employee.id}
  end

  test "should get index" do
    get :index
    assert_response :success
  end

  test "should get new" do
    get :new
    assert_response :success
  end

  test "should get show" do
    get :show, @params
    assert_response :success
  end

  test "should get edit" do
    get :edit, @params
    assert_response :success
  end

  test "should post create" do
    attrs = attributes_for :employee
    attrs[:privilege_ids] = @employee_privilege.id

    post :create, :employee => attrs
    assert_response :redirect

    employee = Employee.last
    assert employee
  end

  test "should put update" do
    first_name = @employee.first_name

    attrs = {}
    attrs[:first_name] = generate :name
    put :update, @params.merge(:employee => attrs)
    assert_response :redirect

    @employee.reload
    assert @employee.first_name != first_name
  end

  test "should put trigger_state_event" do
    put :trigger_state_event, @params.merge(event: :del) # testing only 1 possible event
    assert_response :redirect

    @employee.reload
    assert @employee.deleted?
  end
end
