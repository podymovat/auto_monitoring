$ = jQuery
$.fn.extend
  employee_tab: (options)->
    settings =
      active_tab_class: 'active'

    settings = $.extend settings, options
    return @each ()->
      elem = $('.tab-content ul.nav li.active', this)
      if elem
        # show pane
        pane = elem.closest('.pane')
        # activate parent tab
        $('a[rel="' + pane.attr('id') + '"]').parent().addClass(settings.active_tab_class)

$ ->
  $('#employee-tabs').employee_tab()