class AddKindIdToEmployeePrivileges < ActiveRecord::Migration
  def change
    change_table :employee_privileges do |t|
      t.references :kind
    end
  end
end
