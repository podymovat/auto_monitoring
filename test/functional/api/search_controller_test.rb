require 'test_helper'

class Api::SearchControllerTest < ActionController::TestCase

  def setup
    employee = create(:employee)
    sign_in(employee)
    @params = { :format => :json }
  end

  test "should get index" do
    company = create(:company)
    attrs = { :term => company.to_s }
    get :index, @params.merge(attrs)
    assert_response :success
  end
end
