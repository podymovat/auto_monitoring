class AddPeriodEndDateToCompanyInvoices < ActiveRecord::Migration
  def change
    add_column :company_invoices, :period_end_date, :date
  end
end
