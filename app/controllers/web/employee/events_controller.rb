class Web::Employee::EventsController < Web::Employee::ProtectedApplicationController
  add_i18n_breadcrumb :index, :employee_events_path

  def index
    title t(:employee_events)

    params[:search] = {:meta_sort => 'id.asc'}.merge(params[:search] || {})
    scope = build_scope 'employee/event'
    @search = scope.events_list.metasearch(params[:search])
    @events = @search.page(params[:page]).per(params[:per_page])
  end

  def new
    @event = ::Employee::Event.new(params[:employee_event])
    @event.due_date = Date.today
    @event.creator = current_employee

    add_i18n_breadcrumb :new, :new_employee_event_path
  end

  def show
    scope = build_scope 'employee/event'
    @event = scope.get(params[:id])

    add_breadcrumb t('breadcrumbs.web.employee.events.show', {:company => @event.company}), :employee_event_path
  end

  def create
    @event = ::Employee::Event.new(params[:employee_event])
    @event.creator = current_employee

    if @event.save
      flash[:success] = flash_translate(:success)
      redirect_to employee_company_path(@event.company)
    else
      add_i18n_breadcrumb :new, :new_employee_event_path
      render :action => :new
    end
  end

  def edit
    scope = build_scope 'employee/event'
    @event = scope.get(params[:id])

    add_i18n_breadcrumb :edit, :edit_employee_event_path
  end

  def update
    scope = build_scope 'employee/event'
    @event = scope.get(params[:id])

    add_i18n_breadcrumb :edit, :edit_employee_event_path

    if @event.update_attributes(params[:employee_event])
      flash[:success] = flash_translate(:success)
      redirect_to employee_company_path(@event.company)
    else
      render :action => :edit
    end
  end

  def destroy
    scope = build_scope 'employee/event'
    @event = scope.get(params[:id])
    @event.destroy

    redirect_to :action => :index
  end
end
