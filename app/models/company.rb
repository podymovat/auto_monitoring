class Company < ActiveRecord::Base
  include CompanyRepository

  PHONES_NUMBER_MIN = 1
  EMAILS_NUMBER_MIN = 1

  attr_accessible :name, :state_event, :payment_method_id, :tariff_id, :status_id, :assigned_id,
                  :category_id, :referer, :rental_fee,
                  :contact_details_attributes, :counteragents_attributes, :gis_objects_code_list

  acts_as_taggable_on :gis_objects_codes

  composed_of :rental_fee, ComposeOfParams.money(:rental_fee_in_kop)

  belongs_to :payment_method
  belongs_to :tariff
  belongs_to :category
  belongs_to :status
  belongs_to :creator, :class_name => ::Employee, :foreign_key => 'employee_id'
  belongs_to :assigned, :class_name => ::Employee, :foreign_key => 'assigned_id'

  has_many :contact_details, :as => :contactable, :dependent => :destroy
  has_many :people, :dependent => :destroy
  has_many :invoices, :dependent => :destroy
  has_many :deals, :dependent => :destroy
  has_many :accounts, :dependent => :destroy
  has_many :crm_accounts, :dependent => :destroy
  has_many :gis_accounts, :dependent => :destroy
  has_many :notes, :dependent => :destroy, :order => 'created_at DESC'
  has_many :events, :dependent => :destroy, :class_name => ::Employee::Event
  has_many :counteragents, :dependent => :destroy
  has_many :counteragent_kinds, :through => :counteragents,
             :class_name => 'Counteragent::Kind', :source => :kind, :uniq => true

  validates :name, :presence => true, :uniqueness => {:case_sensitive => false}, :length => {:maximum => 255}
  validates :creator, :presence => true
  validates :assigned, :presence => true
  validates :rental_fee, :numericality => { :greater_than_or_equal_to => 0  }, :allow_nil => true
  validates_each :rental_fee do |record, attr, value|
    record.errors.add(attr, :not_a_number) if value.is_a?(NilMoney)
  end

  validate :contact_details_checker, :unless => :our_client?
  validate :contact_details_our_client_checker, :if => :our_client?

  after_initialize :init

  accepts_nested_attributes_for :contact_details, :allow_destroy => true, :reject_if => :reject_emails
  accepts_nested_attributes_for :counteragents, :allow_destroy => true

  state_machine :state, :initial => :active do
    state :active
  end

  def to_s
    name
  end

  def phones_number_valid?
    kind = ContactDetail::Kind.phone
    phones = contact_details.select { |cd| cd.kind_id == kind.id }
    phones.size >= PHONES_NUMBER_MIN
  end

  def email_number_valid?
    kind = ContactDetail::Kind.email
    emails = contact_details.select { |cd| cd.kind_id == kind.id }
    emails.size >= EMAILS_NUMBER_MIN
  end

  def our_client?
    status.our_client? if status.present?
  end

  def cars_count
    counteragents.collect{ |c| c.cars_count || 0 }.sum
  end

  # Public: Sets nil if value is blank, calls super otherwise
  #
  # value - The String value
  def rental_fee_with_empty_string_processing=(value)
    value = nil if value.is_a?(String) && value.empty?
    self.send('rental_fee_without_empty_string_processing=', value)
  end

  alias_method_chain :rental_fee=, :empty_string_processing

  private

  def init
    if new_record?
      self.rental_fee ||= 0.0
    end
  end

  def contact_details_checker
    unless phones_number_valid?
      errors.add(:base, :phones_too_short, :count => PHONES_NUMBER_MIN)
    end
  end

  def contact_details_our_client_checker
    unless phones_number_valid?
      errors.add(:base, :phones_too_short, :count => PHONES_NUMBER_MIN)
    end
    unless email_number_valid?
      errors.add(:base, :email_too_short, :count => EMAILS_NUMBER_MIN, :our_client_name => our_client_status_name)
    end
  end

  def our_client_status_name
    Status.find(Status::OUR_CLIENT_ID).name
  end

  def reject_emails(attributed)
    unless our_client?
      if attributed['kind_id'] == ContactDetail::EMAIL_ID.to_s && attributed['kind_value_source'].blank?
        return true
      end
    end
  end
end

