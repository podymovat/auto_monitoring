class AddJuridicalPersonIdToCompanyInvoices2 < ActiveRecord::Migration
  def change
    add_column :company_invoices, :juridical_person_id, :integer
  end
end
