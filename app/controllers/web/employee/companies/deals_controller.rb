class Web::Employee::Companies::DealsController < Web::Employee::Companies::ApplicationController
  def show
    scope_deals = build_scope 'company/deal'
    @company_deal = scope_deals.get(params[:id]).decorate
    scope_invoices = build_scope 'company/invoice'
    @invoices = Company::InvoiceDecorator.decorate(scope_invoices.find_all_by_deal_id(@company_deal.id))

    add_i18n_breadcrumb :index, employee_companies_deals_path({:search => {:company_id_equals => current_company.id}})
    add_breadcrumb @company_deal.deal_number, employee_company_deal_path(@company_deal.company, @company_deal)
  end
end
