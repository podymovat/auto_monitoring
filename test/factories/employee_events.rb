FactoryGirl.define do
  factory 'employee/event' do
    company
    creator
    description
    due_date
    association :company_person, :factory => 'company/person'
  end
end
