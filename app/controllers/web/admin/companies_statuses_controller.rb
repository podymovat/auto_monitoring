class Web::Admin::CompaniesStatusesController < Web::Admin::ProtectedApplicationController
  add_i18n_breadcrumb :index, :admin_companies_statuses_path

  def index
    title t(:companies_statuses)

    params[:search] = {:meta_sort => 'id.asc'}.merge(params[:search] || {})
    @search = ::Company::Status.metasearch(params[:search])
    @companies_statuses = @search.page(params[:page]).per(params[:per_page])
  end

  def new
    @company_status = ::Company::Status.new

    add_i18n_breadcrumb :new, :new_admin_companies_status_path
  end

  def create
    @company_status = ::Company::Status.new(params[:company_status])

    if @company_status.save
      flash[:success] = flash_translate(:success)
      redirect_to :action => :index
    else
      add_i18n_breadcrumb :new, :new_admin_companies_status_path
      render :action => :new
    end
  end

  def edit
    @company_status = ::Company::Status.find(params[:id])

    add_breadcrumb @company_status, :edit_admin_companies_status_path
  end

  def update
    @company_status = ::Company::Status.find(params[:id])

    add_breadcrumb @company_status.name, :edit_admin_companies_status_path

    if @company_status.update_attributes(params[:company_status])
      flash[:success] = flash_translate(:success)
      redirect_to :action => :index
    else
      render :action => :edit
    end
  end

  def destroy
    @company_status = ::Company::Status.find(params[:id])
    @company_status.destroy

    redirect_to :action => :index
  end
end
