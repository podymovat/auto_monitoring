class AddCategoryToCompany < ActiveRecord::Migration
  def change
    change_table :companies do |t|
      t.references :company_category
    end
  end
end
