# encoding: UTF-8

[
  {:id => 1, :name => "Наличный"},
  {:id => 2, :name => "Безналичный"},
  {:id => 3, :name => "Квитанция"}
]
.each do |v|
  p = PaymentMethod.find_or_initialize_by_id v[:id]
  p.update_attributes(v)
  p.save
end

Tariff.create([
  {:name => "M-Lite", :cost => 100000},
  {:name => "M-Standard", :cost => 200000},
  {:name => "M-Professional", :cost => 300000},
  {:name => "M-Control", :cost => 400000}
])

[
  {:id => 1, :name => 'Телефон', :slug => :phone },
  {:id => 2, :name => 'Факс', :slug => :fax },
  {:id => 3, :name => 'Адрес', :slug => :address },
  {:id => 4, :name => 'Email', :slug => :email },
  {:id => 5, :name => 'Сайт', :slug => :site },
  {:id => 6, :name => 'Реквизиты', :slug => :details },
  {:id => 7, :name => 'Юр. лицо', :slug => :legal_person },
  {:id => 8, :name => 'Рубрика', :slug => :heading },
  {:id => 9, :name => 'Подрубрика', :slug => :subheading }
].each do |v|
  d = ContactDetail::Kind.find_or_initialize_by_id v[:id]
  d.update_attributes(v)
  d.save
end

[
  {:id => 1, :name => 'Абонентская плата'},
  {:id => 2, :name => 'Обычный'}
].each do |v|
  d = Company::Invoice::Kind.find_or_initialize_by_id v[:id]
  d.update_attributes(v)
  d.save
end

Company::Counteragent::Kind.create([
 {:name => 'Юр. лицо', :abbreviation => 'К'},
 {:name => 'Физ. лицо', :abbreviation => 'Н'},
 {:name => 'Дилер', :abbreviation => 'Д'}
])

[
  {:id => 1, :name => 'Наш клиент', :color => '229605' },
  {:id => 2, :name => 'Потенциальный', :color => '14E3CE' },
  {:id => 3, :name => 'На тесте', :color => 'EBEB07' }
].each do |v|
  s = ::Company::Status.find_or_initialize_by_id v[:id]
  s.update_attributes(v)
  s.save
end


Company::Category.create([
  {:name => 'ЖКХ'},
  {:name => 'Мусорщики'},
  {:name => 'Строители'}
])

# privileges_kind
companies_privilege = Employee::Privilege::Kind.create(name: 'Компании')
invoices_privilege = Employee::Privilege::Kind.create(name: 'Счета')
product_items_privilege = Employee::Privilege::Kind.create(name: 'Товарные позиции')

privileges = Employee::Privilege.create([
  {:name => 'Удаление компании', :key => :company_delete, :kind => companies_privilege},
  {:name => 'Управление всеми компаниями', :key => :company_manage_all, :kind => companies_privilege},
  {:name => 'Управление удалёнными/помещёнными в архив счетами', :key => :invoice_manage_all, :kind => invoices_privilege},
  {:name => 'Установка планируемой даты оплаты счета', :key => :invoice_setup_expected_payment_date, :kind => invoices_privilege},
  {:name => 'Установка фактической даты оплаты счета', :key => :invoice_setup_actual_payment_date, :kind => invoices_privilege},
  {:name => 'Установка даты оплаты счета с типом "наличный"', :key => :invoice_setup_actual_payment_date_with_cash, :kind => invoices_privilege},
  {:name => 'Управление товарными позициями', :key => :product_items_manage, :kind => product_items_privilege}
])

# departments
sales_department = Employee::Department.create(name: 'Отдел продаж')

# positions
Employee::Position.create(name: 'Руководитель', department_id: sales_department.id, privilege_ids: privileges.collect(&:id))
ceo = Employee::Position.create(name: 'Генеральный директор', privilege_ids: privileges.collect(&:id))

Employee.create([
  {:first_name => 'Станислав', :last_name => 'Березинский', :email => 'stanislav@monitoring-auto.ru', :password => '54321', :position_id => ceo.id},
  {:first_name => 'Евгений', :last_name => 'Амурский', :email => 'amur@monitoring-auto.ru', :password => '54321'},
  {:first_name => 'tester', :last_name => 'tester', :email => 'test@test.ru', :password => '54321'}
])

ActiveRecord::Base.connection.execute("SELECT setval('payment_methods_id_seq', max(id)) FROM payment_methods;")
ActiveRecord::Base.connection.execute("SELECT setval('contact_detail_kinds_id_seq', max(id)) FROM contact_detail_kinds;")
ActiveRecord::Base.connection.execute("SELECT setval('company_invoice_kinds_id_seq', max(id)) FROM company_invoice_kinds;")
ActiveRecord::Base.connection.execute("SELECT setval('company_statuses_id_seq', max(id)) FROM company_statuses;")

# juridical_persons
JuridicalPerson::Detail::Kind.create([
  {name: 'Полное наименование юридического лица'},
  {name: 'Сокращенное наименование юридического лица'},
  {name: 'Руководитель компании'},
  {name: 'Юридический адрес'},
  {name: 'Телефон (по фактическому адресу)'},
  {name: 'Факс (по фактическому адресу)'},
  {name: 'e-mail'},
  {name: 'ИНН'},
  {name: 'КПП'},
  {name: 'ОГРН'},
  {name: 'ОКВЭД'},
  {name: 'ОКПО'},
  {name: 'ОКАТО'},
  {name: 'Полное наименование банка'},
  {name: 'Расчетный  счет №'},
  {name: 'Кор.счет №'},
  {name: 'БИК'}
])
