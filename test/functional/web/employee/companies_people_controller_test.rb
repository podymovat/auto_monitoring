require 'test_helper'

class Web::Employee::CompaniesPeopleControllerTest < ActionController::TestCase
  def setup
    @employee = create(:employee)
    sign_in(@employee)
    @company = create(:company, :creator => @employee, :assigned => @employee)
    @company_person = create('company/person', :company => @company)
    @params = {:id => @company_person.id}
  end

  test "should get index" do
    get :index
    assert_response :success
  end

  test "should get new" do
    get :new, {:company_person => {:company_id => @company.id}}
    assert_response :success
  end

  test "should post create" do
    attrs = attributes_for 'company/person'
    attrs[:company_id] = @company.id

    post :create, :company_person => attrs
    assert_response :redirect

    company_person = Company::Person.last
    assert_equal attrs[:name], company_person.name
  end

  test "should put update" do
    attrs = {}
    attrs[:name] = generate :name
    put :update, @params.merge(:company_person => attrs)
    assert_response :redirect

    @company_person.reload
    assert_equal attrs[:name], @company_person.name
  end

  test "should get edit" do
    get :edit, @params
    assert_response :success
  end

  test "should delete destroy" do
    delete :destroy, @params
    assert_response :redirect

    assert !Company::Person.exists?(@params[:id])
  end
end
