class Employee::Privilege < ActiveRecord::Base
  attr_accessible :key, :name, :kind

  belongs_to :kind

  has_many :employee_credentials
  has_many :employees, :through => :employee_credentials
  has_many :position_privileges
  has_many :positions, :through => :position_privileges

  validates :name, :presence => true, :length => {:maximum => 255}
  validates :key, :slug => true, :uniqueness => true
  validates :kind, :presence => true
end
