class AddPaymentMethodIdToCompanyInvoices < ActiveRecord::Migration
  def change
    add_column :company_invoices, :payment_method_id, :integer
  end
end
