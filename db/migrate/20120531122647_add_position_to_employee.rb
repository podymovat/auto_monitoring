class AddPositionToEmployee < ActiveRecord::Migration
  def change
    change_table :employees do |t|
      t.references :position
    end
  end
end
