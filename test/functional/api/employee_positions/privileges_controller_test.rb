require 'test_helper'

class Api::EmployeePositions::PrivilegesControllerTest < ActionController::TestCase

  def setup
    @employee = create(:employee)
    sign_in(@employee)
    @params = { :format => :json }
  end

  test "should get index" do
    attrs = { :employee_position_id => @employee.position_id }
    get :index, @params.merge(attrs)
    assert_response :success
  end
end
