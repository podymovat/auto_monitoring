FactoryGirl.define do
  factory 'company/counteragent/kind' do
    name
    abbreviation
  end
end
