class CreateCompanyDeals < ActiveRecord::Migration
  def change
    create_table :company_deals do |t|
      t.integer :company_id
      t.integer :kind_id
      t.string :deal_number
      t.date :deal_date
      t.integer :total_amount_in_kop
      t.integer :paid_amount_in_kop
      t.string :currency
      t.string :state

      t.timestamps
    end
  end
end
