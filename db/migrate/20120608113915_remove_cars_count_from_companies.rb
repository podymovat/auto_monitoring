class RemoveCarsCountFromCompanies < ActiveRecord::Migration
  def up
    remove_column :companies, :cars_count
  end

  def down
    add_column :companies, :cars_count, :integer
  end
end
