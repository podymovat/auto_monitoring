class RenameProductsToProductItems < ActiveRecord::Migration
  def up
    rename_table :products, :product_items
  end

  def down
    rename_table :product_items, :products
  end
end
