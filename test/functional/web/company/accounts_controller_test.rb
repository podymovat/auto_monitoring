require 'test_helper'

class Web::Company::AccountsControllerTest < ActionController::TestCase
  def setup
    @employee = create(:employee)
    sign_in(@employee)
    @company = create(:company, :creator => @employee, :assigned => @employee)
    @company_person = create('company/person', :creator => @employee, :company => @company)
    @company_crm_account = create('company/crm_account', :company => @company, :person => @company_person)
    employee_sign_out

    sign_in(@company_crm_account)
    @params = {:id => @company_crm_account.id}
  end

  test "should get index" do
    get :index
    assert_response :success
  end
end
