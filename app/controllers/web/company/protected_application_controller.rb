class Web::Company::ProtectedApplicationController < Web::Company::ApplicationController
  before_filter :authenticate_company_account!
end
