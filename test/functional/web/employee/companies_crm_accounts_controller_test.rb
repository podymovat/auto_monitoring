require 'test_helper'

class Web::Employee::CompaniesCrmAccountsControllerTest < ActionController::TestCase
  def setup
    @employee = create(:employee)
    sign_in(@employee)
    @company = create(:company, :creator => @employee, :assigned => @employee)
    @company_person = create('company/person', :creator => @employee, :company => @company)
    @company_crm_account = create('company/crm_account', :company => @company, :person => @company_person)
    @params = {:id => @company_crm_account.id}
  end

  test "should get new" do
    get :new
    assert_response :success
  end

  test "should post create" do
    attrs = attributes_for 'company/crm_account', :company_id => @company.id, :person_id => @company_person.id, :type => ::Company::CrmAccount

    post :create, :company_crm_account => attrs
    assert_response :redirect

    company_crm_account = ::Company::CrmAccount.last
    assert_equal attrs[:login], company_crm_account.login
  end

  test "should get edit" do
    get :edit, @params
    assert_response :success
  end

  test "should put update" do
    attrs = {}
    attrs[:password]= generate :password
    attrs[:login] = generate :login
    put :update, @params.merge(:company_crm_account => attrs)
    assert_response :redirect

    @company_crm_account.reload
    assert_equal attrs[:login], @company_crm_account.login
  end
end
