class Web::Employee::CompaniesAccountsController < Web::Employee::ProtectedApplicationController
  add_i18n_breadcrumb :general, :employee_companies_path
  def index
    title t(:companies_accounts)

    params[:search] = {:meta_sort => 'id.asc'}.merge(params[:search] || {})
    scope = build_scope 'company/account'
    @search = scope.accounts_list.metasearch(params[:search])
    @companies_accounts = @search.page(params[:page]).per(params[:per_page])
    @companies_accounts = Draper::DecoratedEnumerableProxy.new(@companies_accounts, Company::Account)
    add_i18n_breadcrumb :index, :employee_companies_accounts_path
  end

  def destroy
    scope = build_scope 'company/account'
    @company_account = scope.get(params[:id])
    @company_account.destroy
    redirect_to :action => :index
  end
end
