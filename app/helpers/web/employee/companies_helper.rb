module Web::Employee::CompaniesHelper

  def status_label(company)
    status = company.status
    if status
      cls = "label"
      content_tag(:span, status.to_s, { :class => cls , :style => "background-color: ##{status.color}"})
    end
  end

  def phone_kind
    ContactDetail::Kind.phone
  end

  def options_for_contact_detail_kinds(contact_detail)
    options_for_select(contact_detail_kinds.map { |k| [k.to_s, k.id, { rel: k.slug }]}, :selected => contact_detail.kind_id)
  end

  def items_count(items)
    items.present? ? items.count : 0
  end
end
