class PhoneValidator < ActiveModel::EachValidator

  def validate_each(record, attribute, value)
    length = options[:length] || 10
    unless value && value.length == length
      record.errors.add(attribute, :phone, options.merge(:value => value))
    end
  end
end