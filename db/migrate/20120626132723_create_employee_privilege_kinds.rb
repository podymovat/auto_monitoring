class CreateEmployeePrivilegeKinds < ActiveRecord::Migration
  def change
    create_table :employee_privilege_kinds do |t|
      t.string :name
    end
  end
end
