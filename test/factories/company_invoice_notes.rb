FactoryGirl.define do
  factory 'company/invoice/note' do
    association :invoice, :factory => 'company/invoice'
    body
    creator
  end
end
