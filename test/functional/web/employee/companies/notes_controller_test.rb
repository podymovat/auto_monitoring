require 'test_helper'

class Web::Employee::Companies::NotesControllerTest < ActionController::TestCase
  def setup
    @employee = create(:employee)
    sign_in(@employee)
    @company = create(:company, :creator => @employee, :assigned => @employee)
    @params = {:company_id => @company.id}
  end

  test "should post create" do
    attrs = attributes_for 'company/note'

    post :create, @params.merge(:company_note => attrs)
    assert_response :redirect

    note = ::Company::Note.last
    assert_equal attrs[:body], note.body
  end
end
