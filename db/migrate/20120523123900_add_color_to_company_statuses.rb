class AddColorToCompanyStatuses < ActiveRecord::Migration
  def up
    add_column :company_statuses, :color, :string
  end
  def down
    remove_column :company_statuses, :color
  end
end
