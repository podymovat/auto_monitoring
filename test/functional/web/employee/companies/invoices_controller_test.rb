require 'test_helper'

class Web::Employee::Companies::InvoicesControllerTest < ActionController::TestCase
  def setup
    @employee = create(:employee)
    sign_in(@employee)
    @company = create(:company, :creator => @employee, :assigned => @employee)
    @company_invoice = @company.invoices.last
    @params = {:company_id => @company.id, :id => @company_invoice.id}
  end

  test "should get show" do
    get :show, @params
    assert_response :success
  end

  test "should put trigger_state_event" do
    put :trigger_state_event, @params.merge(event: :del) # testing only 1 possible event
    assert_response :redirect

    @company_invoice.reload
    assert @company_invoice.deleted?
  end
end
