require 'test_helper'

class Web::Employee::EventsControllerTest < ActionController::TestCase

  def setup
    @employee = create(:employee)
    sign_in(@employee)
    @company = create(:company, :creator => @employee, :assigned => @employee)
    @company_person = create('company/person', :company => @company)
    @event = create('employee/event', :creator => @employee, :company => @company)
    @params = {:id => @event.id}
  end

  test "should get index" do
    get :index
    assert_response :success
  end

  test "should get new" do
    get :new
    assert_response :success
  end

  test "should get show" do
    get :show, @params
    assert_response :success
  end

  test "should post create" do
    attrs = attributes_for 'employee/event'
    attrs[:company_id] = @company.id

    post :create, :employee_event => attrs
    assert_response :redirect

    event = Employee::Event.last
    assert_equal attrs[:description], event.description
  end

  test "should get edit" do
    get :edit, @params
    assert_response :success
  end

  test "should put update" do
    attrs = {}
    attrs[:description] = generate :name
    put :update, @params.merge(:employee_event => attrs)
    assert_response :redirect

    @event.reload
    assert_equal attrs[:description], @event.description
  end

  test "should delete destroy" do
    delete :destroy, @params
    assert_response :redirect

    assert !Employee::Event.exists?(@params[:id])
  end
end
