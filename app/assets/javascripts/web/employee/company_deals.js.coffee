$ = jQuery
$.fn.extend
  company_deal: (options) ->
    settings =
      dateFormat:
        db: 'yy-mm-dd'

    settings = $.extend settings, options
    return @each ()->
      # datepicker
      $('#company_deal_deal_date', this).datepicker
        numberOfMonths: 2,
        showButtonPanel: true,
        dateFormat: settings.dateFormat.db

      company = $('#company_deal_company_id')
      counteragent = $('#company_deal_counteragent_id')

      # disable selects on startup
      if !company.val()
        counteragent.attr('disabled', 'disabled')

      company.change (event, callback) ->
        if this.value
          counteragent.load(
            Routes.api_company_counteragents_path(this.value),
          ->
            $(this).removeAttr('disabled')
          )
        else
          counteragent.val($('options:first', kind).val());
          counteragent.attr('disabled', 'disabled')
$ ->
  $(document).company_deal()
