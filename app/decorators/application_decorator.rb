class ApplicationDecorator < Draper::Base

  def to_s
    source.to_s
  end
end
