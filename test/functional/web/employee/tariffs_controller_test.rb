require 'test_helper'

class Web::Employee::TariffsControllerTest < ActionController::TestCase
  def setup
    @employee = create(:employee)
    sign_in(@employee)
    @tariff = create(:tariff)
    @params = {:id => @tariff.id}
  end

  test "should get index" do
    get :index
    assert_response :success
  end

  test "should get new" do
    get :new
    assert_response :success
  end

  test "should post create" do
    attrs = attributes_for :tariff

    post :create, :tariff => attrs
    assert_response :redirect

    tariff = Tariff.last
    assert_equal attrs[:name], tariff.name
  end

  test "should get edit" do
    get :edit, @params
    assert_response :success
  end

  test "should put update" do
    attrs = {}
    attrs[:name] = generate :name
    put :update, @params.merge(:tariff => attrs)
    assert_response :redirect

    tariff = Tariff.find(@params[:id])
    assert_equal attrs[:name], tariff.name
  end

  test "should delete destroy" do
    delete :destroy, @params
    assert_response :redirect

    assert !Tariff.exists?(@params[:id])
  end
end
