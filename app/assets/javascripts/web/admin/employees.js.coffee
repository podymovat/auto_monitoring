$ = jQuery
$.fn.extend
  employee: (options) ->
    settings =
      position_change_confirmation_msg: 'В список привилегий будет подгружен новый набор привилегий, согласно выбранной должности. Вы уверены, что хотите продолжить?'

    settings = $.extend settings, options
    return @each ()->
      $('#employee_position_id', this).focus ->
        $(this).data('employee_position_id_old', $(this).val())

      $('#employee_position_id', this).change (event, callback) ->
        if confirm(settings.position_change_confirmation_msg)
          if this.value
            $.ajax(
              url: Routes.api_employee_position_privileges_path(this.value)
              statusCode:
                401: ->
                  window.location.reload()
            ).done((privilege_ids) ->
              $('input[type="checkbox"][name="employee[privilege_ids][]"]').each ->

                if parseInt(this.value) in privilege_ids
                  $(this).attr('checked', 'checked')
                else
                  $(this).removeAttr('checked')
            )
          else
            $('input[type="checkbox"][name="employee[privilege_ids][]"]').each ->
              $(this).removeAttr('checked')
        else
          # set old value
          $(this).val($(this).data('employee_position_id_old'))
        # trigger focus to set a new old value
        $(this).trigger('focus')
$ ->
  $(document).employee()
