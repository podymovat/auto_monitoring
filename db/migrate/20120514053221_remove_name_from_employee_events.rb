class RemoveNameFromEmployeeEvents < ActiveRecord::Migration
  def up
    remove_column :employee_events, :name
  end

  def down
    add_column :employee_events, :name, :string
  end
end
