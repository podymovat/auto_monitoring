class AddUniqueIndexOnNameToTariffs < ActiveRecord::Migration
  def change
    add_index :tariffs, :name, :unique => true
  end
end
