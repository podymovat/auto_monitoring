class Web::Employee::Companies::NotesController < Web::Employee::Companies::ApplicationController
  def create
    @company = current_company
    @note = @company.notes.build(params[:company_note])
    @note.creator = current_employee

    if @note.save
      flash[:success] = flash_translate(:success)
    else
      flash[:error] = flash_translate(:error)
    end

    redirect_to employee_company_path(@company)
  end
end
