require 'active_support/core_ext/array/wrap'

class ContactDetailUniquenessValidator < ActiveRecord::Validations::UniquenessValidator

  def validate_each(record, attribute, value)
    finder_class = find_finder_class_for(record)
    table = finder_class.arel_table

    coder = record.class.serialized_attributes[attribute.to_s]

    if value && coder
      value = coder.dump value
    end

    relation = build_relation(finder_class, table, attribute, value)
    relation = relation.and(table[finder_class.primary_key.to_sym].not_eq(record.send(:id))) if record.persisted?

    Array.wrap(options[:scope]).each do |scope_item|
      scope_value = record.send(scope_item)
      relation = relation.and(table[scope_item].eq(scope_value))
    end

    item = finder_class.unscoped.where(relation)
    if item.exists?
      add_error(item, record, attribute, value)
    end
  end

  private

  def add_error(item, record, attribute, value)
    attrs = options.except(:case_sensitive, :scope).merge(value: value)

    contactable_record = item.first.contactable
    case
      when contactable_record.is_a?(::Company)
        contactable_record_attrs = { company_id: contactable_record.id,
                                     company_name: contactable_record.to_s}
        record.errors.add(attribute, :taken_by_company, attrs.merge(contactable_record_attrs))
      when contactable_record.is_a?(::Company::Person)
        contactable_record_attrs = { company_id: contactable_record.company_id,
                                     company_person_id: contactable_record.id,
                                     company_person_name: contactable_record.to_s }
        record.errors.add(attribute, :taken_by_company_person, attrs.merge(contactable_record_attrs))
      else
        record.errors.add(attribute, :taken, attrs)
    end
  end
end
