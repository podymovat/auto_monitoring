class RenameCompanyDealIdToDealIdOnCompanyInvoices < ActiveRecord::Migration
  def change
    rename_column :company_invoices, :company_deal_id, :deal_id
  end
end
