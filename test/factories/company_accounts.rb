FactoryGirl.define do
  factory 'company/account' do
    login
    company
    creator

    trait :crm do
      password
      association :person, :factory => 'company/person'
      type ::Company::CrmAccount.name
    end

    trait :gis do
      password_text
      type ::Company::GisAccount.name
    end

    factory 'company/crm_account', traits: [:crm], :class => ::Company::CrmAccount
    factory 'company/gis_account', traits: [:gis], :class => ::Company::GisAccount
  end
end
