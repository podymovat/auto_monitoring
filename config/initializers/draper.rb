Draper::DecoratedEnumerableProxy.class_eval do
  def decorated_collection
    @decorated_collection ||= @wrapped_collection.collect { |member| member.decorate(@options) }
  end
end
