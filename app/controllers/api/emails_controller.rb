class Api::EmailsController < Api::ApplicationController

  def exists
    if params[:q]
      result = ContactDetail.emails
      result = result.exclude(params[:id]) if params[:id]
      result = result.find_by_kind_value(params[:q])

      respond_with({ exists: result.present?, error: ErrorMessages.for_contact_detail(result, view_context) })
    end
  end
end
