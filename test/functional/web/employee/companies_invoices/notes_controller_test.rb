require 'test_helper'

class Web::Employee::CompaniesInvoices::NotesControllerTest < ActionController::TestCase

  def setup
    @employee = create(:employee)
    sign_in(@employee)
    @company = create(:company, :creator => @employee, :assigned => @employee)
    @company_invoice = @company.invoices.last

    @params = {:companies_invoice_id => @company_invoice.id}
  end

  test "should post create" do
    attrs = attributes_for 'company/invoice/note'

    post :create, @params.merge(:company_invoice_note => attrs)
    assert_response :redirect

    note = Company::Invoice::Note.last
    assert_equal attrs[:body], note.body
  end
end
