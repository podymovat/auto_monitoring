class Company::Status < ActiveRecord::Base
  include UsefullScopes

  OUR_CLIENT_ID = 1

  attr_accessible :name, :color

  before_destroy :check_company_status_our_client_destroy

  validates :name, :presence => true, :uniqueness => {:case_sensitive => false},
            :length => {:maximum => 255}
  validates :color, :presence => true

  def to_s
    name
  end

  def our_client?
    id == OUR_CLIENT_ID
  end

  protected

  def check_company_status_our_client_destroy
    !our_client?
  end
end
