require 'test_helper'

class Web::Employee::CompaniesInvoicesControllerTest < ActionController::TestCase

  KIND_ID_INVOICE_LICENSE_FEE = ::Company::Invoice::Kind::LICENSE_FEE_ID
  KIND_ID_INVOICE_USUAL = ::Company::Invoice::Kind::USUAL_ID

  def setup
    @employee = create(:employee)
    sign_in(@employee)
    @company = create(:company, :creator => @employee, :assigned => @employee)
    @company_deal = create('company/deal', :company => @company)
    @company_invoice = create('company/invoice', :company => @company,
                                          :deal => @company_deal, :kind_id => KIND_ID_INVOICE_USUAL)
    @company_invoice_license_fee = create('company/invoice', :company => @company,
                                        :deal => @company_deal, :kind_id => KIND_ID_INVOICE_LICENSE_FEE,
                                        :period_start_date => generate(:date), :period_end_date => generate(:date) )
    @params_invoice = {:id => @company_invoice.id}
    @params_invoice_license_fee = {:id => @company_invoice_license_fee.id}
  end

  test "should get index" do
    get :index
    assert_response :success
  end

  test "should get new" do
    get :new
    assert_response :success
  end

  test "should post create invoice" do
    attrs = attributes_for('company/invoice', :company_id => @company.id,
                                       :kind_id => KIND_ID_INVOICE_USUAL,
                                       :deal_id => @company_deal.id)

    post :create, :company_invoice => attrs
    assert_response :redirect

    company_invoice = ::Company::Invoice.last
    assert_equal attrs[:amount], company_invoice.amount_in_kop / 100
  end

  test "should post create invoice_license_fee" do
    attrs = attributes_for('company/invoice',
                                       :company_id => @company.id,
                                       :kind_id => KIND_ID_INVOICE_LICENSE_FEE,
                                       :deal_id => @company_deal.id)
    attrs[:period_start_date] = generate(:date)
    attrs[:period_end_date] = generate(:date)

    post :create, :company_invoice => attrs
    assert_response :redirect

    company_invoice = ::Company::Invoice.last
    assert_equal attrs[:amount], company_invoice.amount_in_kop / 100
  end

  test "should put update invoice" do
    attrs = {}
    attrs[:amount] = generate(:integer)
    attrs[:old_type] = @company_invoice.type
    put :update, @params_invoice.merge(:company_invoice => attrs)
    assert_response :redirect

    @company_invoice.reload
    assert_equal attrs[:amount], @company_invoice.amount_in_kop / 100
  end

  test "should put update invoice to invoice_license_fee" do
    attrs = {}
    attrs[:old_type] = @company_invoice.type
    attrs[:kind_id] = KIND_ID_INVOICE_LICENSE_FEE
    attrs[:period_start_date] = generate(:date)
    attrs[:period_end_date] = generate(:date)
    put :update, @params_invoice.merge(:company_invoice => attrs)
    assert_response :redirect

    @company_invoice.reload
    assert_equal attrs[:kind_id], @company_invoice.kind_id
  end

  test "should put update invoice_license_fee to invoice" do
    attrs = {}
    attrs[:old_type] = @company_invoice_license_fee.type
    attrs[:kind_id] = KIND_ID_INVOICE_USUAL
    put :update, @params_invoice_license_fee.merge(:company_invoice => attrs)
    assert_response :redirect

    @company_invoice_license_fee.reload
    assert_equal attrs[:kind_id], @company_invoice_license_fee.kind_id
  end

  test "should get edit" do
    get :edit, @params_invoice
    assert_response :success
  end
end
