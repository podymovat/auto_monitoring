class AddPersonIdToCompanyAccounts < ActiveRecord::Migration
  def change
    change_table :company_accounts do |t|
      t.references :person
    end
  end
end
