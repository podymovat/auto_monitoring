class AddKindValueSourceToContactDetails < ActiveRecord::Migration
  def change
    add_column :contact_details, :kind_value_source, :string
  end
end
