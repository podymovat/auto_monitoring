module Web::Employee::CompaniesInvoicesHelper

  def company_invoice_payment_states
    ::Company::Invoice.state_machine(:payment_state).states.map {|s| [s.human_name, s.name]}
  end

  def company_invoice_states
    ::Company::Invoice.state_machine(:state).states.map {|s| [s.human_name, s.name]}
  end

  def collection_months
    (1..12).collect {|x| [I18n.t("date.month_names")[x], x]}
  end
end
