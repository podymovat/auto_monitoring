class Web::Employee::CompaniesInvoices::ApplicationController < Web::Employee::ProtectedApplicationController
  protected

  def current_company_invoice
    scope = build_scope 'company/invoice'
    scope.get(params[:companies_invoice_id])
  end
end
