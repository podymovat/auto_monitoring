class Company::GisAccount < Company::Account
  attr_accessible :password_text

  validates :password_text, :presence => true, :length => {:maximum => 255}
end
