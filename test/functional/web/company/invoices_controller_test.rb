require 'test_helper'

class Web::Company::InvoicesControllerTest < ActionController::TestCase
  def setup
    @employee = create(:employee)
    sign_in(@employee)
    @company = create(:company, :creator => @employee, :assigned => @employee)
    @company_deal = create('company/deal', :company => @company)
    @company_invoice = create('company/invoice', :company => @company, :deal => @company_deal)
    @company_person = create('company/person', :creator => @employee, :company => @company)
    @company_crm_account = create('company/crm_account', :company => @company, :person => @company_person)
    employee_sign_out

    sign_in(@company_crm_account)
    @params = {:id => @company_invoice.id}
  end

  test "should get index" do
    get :index
    assert_response :success
  end

  test "should get show" do
    get :show, @params
    assert_response :success
  end
end
