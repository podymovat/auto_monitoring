class NewCompanyInvoiceType < Company::Invoice
  include BaseType

  attr_accessible :period_start_date, :period_end_date, :old_type
  attr_accessor :old_type
  cattr_accessor :new_sti_name

  with_options :if => :license_fee? do |assoc|
    assoc.validates :period_start_date, :presence => true
    assoc.validates :period_end_date, :presence => true
  end

  def license_fee?
    kind_id == ::Company::Invoice::Kind::LICENSE_FEE_ID
  end

  def period_start_date=(value)
    if license_fee?
      write_attribute(:period_start_date, value)
    else
      write_attribute(:period_start_date, nil)
    end
  end

  def period_end_date=(value)
    if license_fee?
      write_attribute(:period_end_date, value)
    else
      write_attribute(:period_end_date, nil)
    end
  end

  def self.sti_name
    self.new_sti_name || super
  end

  before_validation do
    self.type = license_fee? ? Company::InvoiceLicenseFee.name : Company::Invoice.name
  end

  before_save do
    self.new_sti_name = old_type
  end
end
