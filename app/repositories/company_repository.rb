module CompanyRepository
  extend ActiveSupport::Concern
  include UsefullScopes

  included do
    has_many :unassigned_events, :dependent => :destroy, :class_name => ::Employee::Event,
             :conditions => { :company_person_id => nil }

    scope :ilike_by_contact_details_entry, lambda { |value|
      joins(:contact_details)
      .where('name ILIKE :value OR
                contact_details.kind_value ILIKE :value',
             :value => "%#{value}%")
      .group('companies.id')
    }

    scope :active, where(:state => :active)
    scope :assigned_to, lambda { |value| where(:assigned_id => value)}
  end

  module ClassMethods
    def companies_tags_names(term)
      tag_counts_on(:gis_objects_codes).named_like(term).select(:name)
    end
  end
end
