$ = jQuery
$.fn.extend
  company: () ->
    settings =
      minLength: 1

    return @each ()->
      $('.company .embedded-block a[data-toggle="block"]', this).click (event) ->
        event.preventDefault()
        target = $(this).attr('href')
        $(this).closest('.embedded-block').find('.block-content ' + target).slideToggle()
        $(this).toggleClass('dropup')

      gis_list = $('#company_gis_objects_code_list')
      split = (val) ->
        val.split /,\s*/
      extractLast = (term) ->
        split(term).pop()
      gis_list.bind("keydown", (event) ->
        event.preventDefault() if event.keyCode is $.ui.keyCode.TAB and $(this).data("autocomplete").menu.active
      ).autocomplete
        minLength: settings.minLength
        source: (request, response) ->
          lastTerm = $.trim(extractLast(request.term))
          if !lastTerm
            return
          params =
            q: lastTerm
          $.ajax(
            url: Routes.autocomplete_api_companies_tags_path()
            data: params
            statusCode:
              401: ->
                window.location.reload()
          ).done((data) ->
            response $.ui.autocomplete.filter(data, extractLast(request.term))
          )
        focus: ->
          false
        select: (event, ui) ->
          terms = split(@value)
          terms.pop()
          terms.push ui.item.value
          terms.push ""
          @value = terms.join(", ")
          false

$ ->
  $(document).company()
