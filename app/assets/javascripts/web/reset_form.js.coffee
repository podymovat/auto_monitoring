$ = jQuery
$.fn.extend
  ###
  # Extends standard reset button functionality,
  # so it become possible to clear ALL inputs
  ###
  reset_form: ()->
    return @each ()->
      $('input[type="reset"]', this).click (event) ->
        event.preventDefault()
        form = $(this).closest('form')
        form.find('input[type="text"]').val('')
        form.find('select').each ->
          $(this).val($('options.first', this))
        form.find('input[type="checkbox"]').removeAttr('checked').removeAttr('selected')
        form.submit()
$ ->
  $(document).reset_form()
