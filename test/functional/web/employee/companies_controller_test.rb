require 'test_helper'

class Web::Employee::CompaniesControllerTest < ActionController::TestCase
  def setup
    @employee = create(:employee)
    @employee_other = create(:employee)
    sign_in(@employee)
    @tariff = create(:tariff)
    @company = create(:company, :creator => @employee, :assigned => @employee)
    @company_other = create(:company, :creator => @employee_other, :assigned => @employee_other)
    @params = {:id => @company.id}
  end

  test "should get index" do
    get :index
    assert_response :success
  end

  test "should get new" do
    get :new
    assert_response :success
  end

  test "should get show my_company" do
    get :show, @params
    assert_response :success
  end

  test "should get show no my_company" do
    get :show, :id => @company_other.id
    assert_response :redirect
  end

  test "should post create" do
    attrs = attributes_for(:company, :assigned_id => @employee.id)
    # add 1 phone at least
    kind_phone = ContactDetail::Kind.phone
    kind_email = ContactDetail::Kind.email
    attrs[:contact_details_attributes] = [{ :kind_id => kind_phone.id, :kind_value_source => generate(:phone) },
                                          { :kind_id => kind_email.id, :kind_value_source => generate(:email) }]

    post :create, :company => attrs
    assert_response :redirect

    company = ::Company.last
    assert_equal attrs[:name], company.name
  end

  test "should get edit" do
    get :edit, @params
    assert_response :success
  end

  test "should put update" do
    attrs = {}
    attrs[:name] = generate :name
    put :update, @params.merge(:company => attrs)
    assert_response :redirect

    @company.reload
    assert_equal attrs[:name], @company.name
  end

  test "should not delete destroy without privilege company_delete" do
    delete :destroy, @params
    assert_response :redirect

    assert ::Company.exists?(@params[:id])
  end

  test "should delete destroy with privilege company_delete" do
    @privilege_company_delete = ::Employee::Privilege.find_by_key(:company_delete)
    @employee.privileges << @privilege_company_delete
    delete :destroy, @params
    assert_response :redirect

    assert !::Company.exists?(@params[:id])
  end
end
