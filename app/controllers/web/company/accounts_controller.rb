class Web::Company::AccountsController < Web::Company::ProtectedApplicationController
  add_i18n_breadcrumb :general, :company_accounts_path
  def index
    title t(:companies_accounts)

    params[:search] = {:meta_sort => 'id.asc'}.merge(params[:search] || {})
    @search = current_company.accounts.metasearch(params[:search])
    @accounts = @search.page(params[:page]).per(params[:per_page])
    @accounts = Draper::DecoratedEnumerableProxy.new(@accounts, Company::Account)
  end
end
