class CompanyDecorator < ApplicationDecorator
  decorates :company

  def rental_fee
    source.rental_fee.present? ? source.rental_fee.format : ''
  end

  def short_name
    name.truncate(25)
  end
end
