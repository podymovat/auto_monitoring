require 'test_helper'

class Api::CompaniesTagsControllerTest < ActionController::TestCase

  def setup
    @employee = create(:employee)
    sign_in(@employee)
    @company = create(:company, :creator => @employee, :gis_objects_code_list => "code")
    @params = { :format => :json}
  end

  test "should get autocomplete" do
    attrs = { :q => "code" }
    get :autocomplete, @params.merge(attrs)
    assert_response :success
  end
end
