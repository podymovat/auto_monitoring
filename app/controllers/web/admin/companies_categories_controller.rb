class Web::Admin::CompaniesCategoriesController < Web::Admin::ProtectedApplicationController
  add_i18n_breadcrumb :index, :admin_companies_categories_path

  def index
    title t(:companies_categories)

    params[:search] = {:meta_sort => 'id.asc'}.merge(params[:search] || {})
    @search = ::Company::Category.metasearch(params[:search])
    @companies_categories = @search.page(params[:page]).per(params[:per_page])
  end

  def new
    @company_category = ::Company::Category.new

    add_i18n_breadcrumb :new, :new_admin_companies_category_path
  end

  def create
    @company_category = ::Company::Category.new(params[:company_category])

    if @company_category.save
      flash[:success] = flash_translate(:success)
      redirect_to :action => :index
    else
      add_i18n_breadcrumb :new, :new_admin_companies_category_path
      render :action => :new
    end
  end

  def edit
    @company_category = ::Company::Category.find(params[:id])

    add_breadcrumb @company_category, :edit_admin_companies_category_path
  end

  def update
    @company_category = ::Company::Category.find(params[:id])

    add_breadcrumb @company_category.name, :edit_admin_companies_category_path

    if @company_category.update_attributes(params[:company_category])
      flash[:success] = flash_translate(:success)
      redirect_to :action => :index
    else
      render :action => :edit
    end
  end

  def destroy
    @company_category = ::Company::Category.find(params[:id])
    @company_category.destroy

    redirect_to :action => :index
  end
end
