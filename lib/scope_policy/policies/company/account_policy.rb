class ScopePolicy::Policies::Company::AccountPolicy < ScopePolicy::Policies::Company::BasePolicy

  def model_relation
    @relation = ::Company::Account.scoped
  end

  def accounts_list
    if !@current_user.can?(:company_manage_all) || @options[:show_assigned_to_me]
      @relation = @relation.company_assigned_to(@current_user)
    end
    @relation
  end
end
