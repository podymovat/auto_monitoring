require 'test_helper'

class Web::Admin::JuridicalPeopleControllerTest < ActionController::TestCase

  def setup
    @signed_in_employee = create(:employee)
    sign_in(@signed_in_employee)
    @juridical_person = create('juridical_person')
    @params = {:id => @juridical_person.id}
  end

  test "should get index" do
    get :index
    assert_response :success
  end

  test "should get new" do
    get :new
    assert_response :success
  end

  test "should get edit" do
    get :edit, @params
    assert_response :success
  end

  test "should post create" do
    attrs = attributes_for('juridical_person')

    post :create, :juridical_person => attrs
    assert_response :redirect

    juridical_person = ::JuridicalPerson.last
    assert juridical_person
  end

  test "should put update" do
    name = @juridical_person.name

    attrs = {}
    attrs[:name] = generate(:name)
    put :update, @params.merge(:juridical_person => attrs)
    assert_response :redirect

    @juridical_person.reload
    assert_not_equal name, @juridical_person.name
  end

  test "should delete destroy" do
    delete :destroy, @params
    assert_response :redirect

    assert !JuridicalPerson.exists?(@juridical_person.id)
  end
end
