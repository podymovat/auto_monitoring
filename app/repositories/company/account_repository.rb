module Company::AccountRepository
  extend ActiveSupport::Concern
  include UsefullScopes

  included do
    scope :active, where(:state => :active)
    scope :company_assigned_to, lambda { |value| joins(:company).where(:companies => {:assigned_id => value})}
  end
end
