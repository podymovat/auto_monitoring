class RemoveJuridicalPersonIdFromCompanyInvoices < ActiveRecord::Migration
  def up
    remove_column :company_invoices, :juridical_person_id
  end

  def down
    add_column :company_invoices, :juridical_person_id, :integer
  end
end
