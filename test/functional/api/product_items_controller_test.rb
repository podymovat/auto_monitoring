require 'test_helper'

class Api::ProductItemsControllerTest < ActionController::TestCase

  def setup
    @employee = create(:employee)
    sign_in(@employee)
    @product_item = create(:product_item, :creator => @employee)
    @params = { :format => :json}
  end

  test "should get show" do
    attrs = { :id => @product_item.id }
    get :show, @params.merge(attrs)
    assert_response :success
  end
end
