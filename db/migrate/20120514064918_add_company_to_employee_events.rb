class AddCompanyToEmployeeEvents < ActiveRecord::Migration
  def change
    change_table :employee_events do |t|
      t.references :company
    end
  end
end
