class Web::Employee::ProtectedApplicationController < Web::Employee::ApplicationController
  before_filter :authenticate_employee!
  before_filter :check_for_employee_constraints

  private

  def check_for_employee_constraints
    constraints = params.slice(:show_assigned_to_me)
    if constraints.present?
      clean_constraints = {}
      constraints.each do |k, v|
        clean_constraints[k.to_sym] = v unless 'false' == v
      end

      save_employee_constraints(clean_constraints)
      redirect_to url_for(params.except(*constraints.keys))
    end
  end
end
