class Company::InvoiceLicenseFee < Company::Invoice
  attr_accessible :period_start_date, :period_end_date

  validates :period_start_date, :presence => true
  validates :period_end_date, :presence => true
end
