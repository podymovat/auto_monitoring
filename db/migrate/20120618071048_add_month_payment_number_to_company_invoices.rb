class AddMonthPaymentNumberToCompanyInvoices < ActiveRecord::Migration
  def change
    change_table :company_invoices do |t|
      t.integer :month_payment_number
    end
  end
end
