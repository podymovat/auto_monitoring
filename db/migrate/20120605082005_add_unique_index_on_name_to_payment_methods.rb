class AddUniqueIndexOnNameToPaymentMethods < ActiveRecord::Migration
  def change
    add_index :payment_methods, :name, :unique => true
  end
end
