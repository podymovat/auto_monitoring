module ActiveRecord
  module ConnectionAdapters
    class PostgreSQLAdapter

      # Returns the next value of the sequence.
      def next_sequence_value(sequence_name) #:nodoc:
        r = exec_query("SELECT nextval($1)", 'SQL', [[nil, sequence_name]])
        Integer(r.rows.first.first)
      end
    end
  end
end