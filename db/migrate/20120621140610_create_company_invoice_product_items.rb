class CreateCompanyInvoiceProductItems < ActiveRecord::Migration
  def change
    create_table :company_invoice_product_items do |t|
      t.references :invoice
      t.references :product_item
      t.integer :product_items_count
      t.integer :prime_amount_in_kop
      t.integer :sail_amount_in_kop
      t.string :currency
      t.string :state

      t.timestamps
    end
  end
end
