FactoryGirl.define do
  factory :contact_detail do
    association :kind, :factory => 'contact_detail/kind'
    kind_value_source
  end
end
