$ = jQuery
$.fn.extend
  event: (options) ->
    settings =
      dateFormat:
        db: 'yy-mm-dd'

    settings = $.extend settings, options

    return @each ()->
      # datepicker
      $('#employee_event_due_date', this).datepicker
        minDate: '-0d',
        numberOfMonths: 2,
        showButtonPanel: true,
        dateFormat: settings.dateFormat.db

      company = $('#employee_event_company_id')
      person = $('#employee_event_company_person_id')

      # disable person select on startup
      if !company.val()
        person.attr('disabled', 'disabled')

      company.change (event, callback) ->
        if this.value
          person.load(
            Routes.api_company_people_path(this.value),
            ->
              $(this).removeAttr('disabled')
          )
        else
          person.val($('options:first', person).val());
          person.attr('disabled', 'disabled')
$ ->
  $(document).event()
