require 'test_helper'

class CompanyTest < ActiveSupport::TestCase
  def setup
    @employee = FactoryGirl.create(:employee)
    sign_in(@employee)
    @tariff = FactoryGirl.create(:tariff)
    @company = FactoryGirl.create(:company, :creator => @employee)
  end

end
