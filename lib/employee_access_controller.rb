module EmployeeAccessController

  class << self
    def can_access_company?(employee, company)
      employee.can?(:company_manage_all) ? true : company.assigned == employee
    end

    def can_access_company_dependent_objects?(employee, object)
      can_access_company?(employee, object.company)
    end

    def can_access_company_invoice?(employee, invoice)
      result = can_access_company_dependent_objects?(employee, invoice)
      unless invoice.active?
        result &= employee.can?(:invoice_manage_all)
      end
      result
    end

    def can_setup_company_invoice_expected_payment_date?(employee, invoice)
      employee.can?(:invoice_setup_expected_payment_date)
    end

    def can_setup_company_invoice_actual_payment_date?(employee, invoice)
      employee.can?(:invoice_setup_actual_payment_date) ||
          (invoice.with_cash? && employee.can?(:invoice_setup_actual_payment_date_with_cash))
    end
  end
end