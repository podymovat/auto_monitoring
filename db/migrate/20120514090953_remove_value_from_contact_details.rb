class RemoveValueFromContactDetails < ActiveRecord::Migration
  def up
    remove_column :contact_details, :value
  end

  def down
    add_column :contact_details, :value, :string
  end
end
