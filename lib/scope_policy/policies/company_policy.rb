class ScopePolicy::Policies::CompanyPolicy < ScopePolicy::Policies::BasePolicy

  def model_relation
    @relation = ::Company.scoped
  end

  def companies_list
    if !@current_user.can?(:company_manage_all) || @options[:show_assigned_to_me]
      @relation = @relation.assigned_to(@current_user)
    end
    @relation
  end

  def get(id)
    @company = @relation.find(id)

    if EmployeeAccessController.can_access_company?(@current_user, @company)
      @company
    else
      raise Employee::Privilege::ObjectAccessDeniedError.new("Access denied for #{@company.class.model_name}", @company)
    end
  end
end
