class AddPeriodStartDateToCompanyInvoices < ActiveRecord::Migration
  def change
    add_column :company_invoices, :period_start_date, :date
  end
end
