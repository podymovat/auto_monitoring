class Api::CompaniesTagsController < Api::ApplicationController
  def autocomplete
    if params[:q].present?
      companies_tags = Company.companies_tags_names(params[:q]).map { |c| c.name }
      respond_with(companies_tags)
    end
  end
end
