module Web::Admin::EmployeesHelper
  def positions_by_department
    options = []
    options << [ t('.without_department'), ::Employee::Position.without_department.map {|p| [p.to_s, p.id]}] if ::Employee::Position.without_department.present?
    ::Employee::Department.find_each do |d|
      options << [ d.name, ::Employee::Position.with_department(d.id).map {|p| [p.to_s, p.id]}]
    end
    options
  end

  def employee_privileges_collection
    options = {}
    ::Employee::Privilege::Kind.find_each do |k|
      options[k.name] = k.privileges.map {|p| [p.name, p.id]}
    end
    options
  end

  def employee_states
    ::Employee.state_machine(:state).states.map {|s| [s.human_name, s.name]}
  end
end
