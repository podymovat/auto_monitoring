$ = jQuery
$.fn.extend
  company_crm_account: (options) ->

    return @each ()->
      company = $('#company_crm_account_company_id')
      person = $('#company_crm_account_person_id')

      # disable person select on startup
      if !company.val()
        person.attr('disabled', 'disabled')

      company.change (event, callback) ->
        if this.value
          person.load(
            Routes.api_company_people_path(this.value),
            ->
              $(this).removeAttr('disabled')
          )
        else
          person.val($('options:first', person).val());
          person.attr('disabled', 'disabled')
$ ->
  $(document).company_crm_account()
