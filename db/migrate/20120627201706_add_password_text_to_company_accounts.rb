class AddPasswordTextToCompanyAccounts < ActiveRecord::Migration
  def change
    add_column :company_accounts, :password_text, :string
  end
end
