class Api::EmployeePositions::ApplicationController < Api::ApplicationController
  protected

  def employee_position
    ::Employee::Position.find(params[:employee_position_id])
  end
end
