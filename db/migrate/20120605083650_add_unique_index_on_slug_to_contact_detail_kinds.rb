class AddUniqueIndexOnSlugToContactDetailKinds < ActiveRecord::Migration
  def change
    add_index :contact_detail_kinds, :slug, :unique => true
  end
end
