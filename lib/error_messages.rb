module ErrorMessages

  def self.for_contact_detail(contact_detail, view_context)
    return nil unless contact_detail
    contactable = contact_detail.contactable
    case
      when contactable.is_a?(::Company)
        I18n.t('errors.messages.taken_by_company',
          link: view_context.link_to(contactable,
                                     view_context.employee_company_path(contactable)))
      when contactable.is_a?(::Company::Person)
        I18n.t('errors.messages.taken_by_company_person',
          link: view_context.link_to(contactable,
                                     view_context.employee_company_person_path(contactable.company, contactable)))
      else
        I18n.t('errors.messages.taken')
    end
  end
end
