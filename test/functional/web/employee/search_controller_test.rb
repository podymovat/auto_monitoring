require 'test_helper'

class Web::Employee::SearchControllerTest < ActionController::TestCase

  def setup
    employee = create(:employee)
    sign_in(employee)
  end

  test "should get index" do
    get :index
    assert_response :success
  end

  test "should search for a company" do
    company = create(:company)
    attrs = { :term => company.to_s }
    get :index, attrs
    assert_response :success
  end
end
