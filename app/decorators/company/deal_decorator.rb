class Company::DealDecorator < ApplicationDecorator
  decorates 'company/deal'

  def total_amount
    source.total_amount.present? ? source.total_amount.format : ''
  end

  def paid_amount
    source.paid_amount.present? ? source.paid_amount.format : ''
  end
end
