FactoryGirl.define do
  factory 'juridical_person/detail' do
    juridical_person
    association :kind, :factory => 'juridical_person/detail/kind'
    value
  end
end
