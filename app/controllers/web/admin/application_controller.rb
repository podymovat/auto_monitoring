class Web::Admin::ApplicationController < Web::ApplicationController
  layout 'web/admin/application'

  before_filter :admin_title
  helper_method :employee_signed_in?, :current_employee

  private

  def admin_title
    title t("title.admin")
  end
end
