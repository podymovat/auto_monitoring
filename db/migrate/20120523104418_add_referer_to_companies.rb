class AddRefererToCompanies < ActiveRecord::Migration
  def change
    add_column :companies, :referer, :string
  end
end
