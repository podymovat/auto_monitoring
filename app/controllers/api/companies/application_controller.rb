class Api::Companies::ApplicationController < Api::ApplicationController
  protected

  def company
    ::Company.find(params[:company_id])
  end
end
