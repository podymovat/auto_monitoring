class RenameEmployeeIdToCreatorId < ActiveRecord::Migration
  def change
    rename_column :company_accounts, :employee_id, :creator_id
  end
end
