class CreateJuridicalPersonDetails < ActiveRecord::Migration
  def change
    create_table :juridical_person_details do |t|
      t.integer :juridical_person_id
      t.integer :kind_id
      t.string :value
      t.string :state

      t.timestamps
    end
  end
end
