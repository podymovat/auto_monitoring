class Api::EmployeePositions::PrivilegesController < Api::EmployeePositions::ApplicationController

  def index
    respond_with(employee_position.privileges.collect(&:id))
  end
end
