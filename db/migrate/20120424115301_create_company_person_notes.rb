class CreateCompanyPersonNotes < ActiveRecord::Migration
  def change
    create_table :company_person_notes do |t|
      t.integer :company_person_id
      t.text :body
      t.integer :employee_id
      t.string :state

      t.timestamps
    end

    add_index :company_person_notes, :company_person_id
    add_index :company_person_notes, :employee_id
  end
end
