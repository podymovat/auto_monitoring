class Web::Employee::ProductItemsController < Web::Employee::ProtectedApplicationController
  add_i18n_breadcrumb :index, :employee_product_items_path
  before_filter :access_product_items

  def index
    title t(:product_items)

    params[:search] = {:meta_sort => 'id.asc'}.merge(params[:search] || {})
    @search = ProductItem.metasearch(params[:search])
    @product_items = ProductItemDecorator.decorate(@search.page(params[:page]).per(params[:per_page]))
  end

  def new
    @product_item = ProductItem.new
    @product_item.creator = current_employee

    add_i18n_breadcrumb :new, :new_employee_product_item_path
  end

  def create
    @product_item = ProductItem.new(params[:product_item])
    @product_item.creator = current_employee

    if @product_item.save
      flash[:success] = flash_translate(:success)
      redirect_to :action => :index
    else
      add_i18n_breadcrumb :new, :new_employee_product_item_path
      render :action => :new
    end
  end

  def edit
    @product_item = ProductItem.find(params[:id])

    add_breadcrumb @product_item, :edit_employee_product_item_path
  end

  def update
    @product_item = ProductItem.find(params[:id])

    add_i18n_breadcrumb :edit, :edit_employee_product_item_path

    if @product_item.update_attributes(params[:product_item])
      flash[:success] = flash_translate(:success)
      redirect_to :action => :index
    else
      render :action => :edit
    end
  end

  def destroy
    @product_item = ProductItem.find(params[:id])
    @product_item.destroy
    redirect_to :action => :index
  end

  protected

  def access_product_items
    unless current_employee.can? :product_items_manage
      raise ::Employee::Privilege::AccessDeniedError, "Access denied for ProductItem."
    end
  end
end
