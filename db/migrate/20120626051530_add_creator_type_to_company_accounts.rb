class AddCreatorTypeToCompanyAccounts < ActiveRecord::Migration
  def change
    add_column :company_accounts, :creator_type, :string
  end
end
