module Company::DealRepository
  extend ActiveSupport::Concern
  include UsefullScopes

  included do
    scope :company_assigned_to, lambda { |value| joins(:company).where(:companies => {:assigned_id => value})}
  end
end
