module ApplicationHelper
  include ScopePolicy::Helper

  def flash_class(level)
    case level
      when :notice then 'alert-notice'
      when :error then 'alert-error'
      when :success then 'alert-success'
    end
  end

  def item(tag, name, path, link_options = {})
    options = {}
    options[:class] = :active if current_page?(path)
    link = link_to(name, path, link_options)
    content_tag(tag, link, options)
  end

  # models
  def companies
    scope = build_scope 'company'
    scope.companies_list.asc_by_name
  end

  def company_categories
    Company::Category.asc_by_name
  end

  def company_statuses
    ::Company::Status.asc_by_name
  end

  def company_invoice_kinds
    ::Company::Invoice::Kind.asc_by_name
  end

  def contact_detail_kinds
    ContactDetail::Kind.asc_by_name
  end

  def employees
    ::Employee.asc_by_full_name
  end

  def unassigned_employee_events(company)
    company.unassigned_events.asc_by_due_date
  end

  def counteragent_kinds
    ::Company::Counteragent::Kind.asc_by_name
  end

  def product_items
    ProductItem.all
  end

  def juridical_people
    JuridicalPerson.asc_by_name
    end

  def juridical_person_detail_kinds
    JuridicalPerson::Detail::Kind.asc_by_name
  end

  # errors
  def error_class(object, cls = :error)
    cls if object.respond_to?(:errors) && object.errors.present?
  end

  # pagination
  def paginate_per_page_values
    configus.pagination.per_page_values
  end

  def paginate_per_page_item(tag, value, path, link_options = {})
    options = {}
    options[:class] = :active if current_per_page.to_i == value

    link = link_to(value, path, link_options)
    content_tag(tag, link, options)
  end

  private

  def current_per_page
    params[:per_page] || Kaminari.config.default_per_page
  end

  # menu-left
  def employee_events_expired
    scope = build_scope 'employee/event'
    scope.events_list.expired
  end

  def employee_events_today
    scope = build_scope 'employee/event'
    scope.events_list.today
  end
end
