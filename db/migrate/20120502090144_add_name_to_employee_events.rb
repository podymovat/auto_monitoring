class AddNameToEmployeeEvents < ActiveRecord::Migration
  def change
    add_column :employee_events, :name, :string
  end
end
