FactoryGirl.define do
  factory 'company/invoice' do
    company
    creator
    association :kind, :factory => 'company/invoice/kind'
    amount
    equity_amount
    invoice_number
    association :deal, :factory => 'company/deal'
    payment_method
    type ::Company::Invoice.name

    after(:create) do |i|
      FactoryGirl.create 'company/invoice/note', invoice: i
      FactoryGirl.create('company/invoice_product_item', invoice: i)
    end
  end
end
