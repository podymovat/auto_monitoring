class AddStateToCompanyPersons < ActiveRecord::Migration
  def change
    add_column :company_people, :state, :string
  end
end
