class AddTypeToCompanyAccounts < ActiveRecord::Migration
  def change
    add_column :company_accounts, :type, :string
  end
end
