class AddKindValueToContactDetails < ActiveRecord::Migration
  def change
    add_column :contact_details, :kind_value, :string
  end
end
