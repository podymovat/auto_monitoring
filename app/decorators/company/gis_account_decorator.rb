class Company::GisAccountDecorator < Company::AccountDecorator
  decorates 'company/gis_account'

  def account_type
    I18n.t('.activerecord.attributes.company/account.gis_account')
  end
end
