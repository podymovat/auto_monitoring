FactoryGirl.define do
  factory 'company/invoice_product_item' do
    association :invoice, :factory => 'company/invoice'
    product_item
    product_items_count
    prime_amount
    sail_amount
  end
end
