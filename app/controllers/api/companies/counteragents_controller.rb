class Api::Companies::CounteragentsController < Api::Companies::ApplicationController
  def index
    counteragents = company.counteragents.asc_by_name
    @grouped_counteragents = counteragents.group_by &:kind
  end
end
