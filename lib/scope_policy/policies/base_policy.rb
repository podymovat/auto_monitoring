class ScopePolicy::Policies::BasePolicy

  # Initialize a new BasePolicy object for a given user
  #
  # == Parameters:
  # current_user::
  #   The current user.
  # options::
  #   User constraints. Supported options are:
  #     * show_assigned_to_me - shows only items, assigned to the user (default: false)
  #
  # == Returns:
  # A new instance of BasePolicy.
  #
  def initialize(current_user, options = {})
    @relation = model_relation
    @current_user = current_user
    @options = options
  end

  def method_missing(name, *args, &block)
    @relation.send(name, *args, &block)
  end
end
