class AddActualPaymentDateToCompanyInvoices < ActiveRecord::Migration
  def change
    add_column :company_invoices, :actual_payment_date, :date
  end
end
