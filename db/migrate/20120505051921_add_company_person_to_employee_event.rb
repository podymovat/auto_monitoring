class AddCompanyPersonToEmployeeEvent < ActiveRecord::Migration
  def change
    change_table :employee_events do |t|
      t.references :company_person
    end
  end
end
