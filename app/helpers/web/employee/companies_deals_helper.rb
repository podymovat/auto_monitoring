module Web::Employee::CompaniesDealsHelper

  def companies_deals_collection
    @company_deals || []
  end

  def counteragents_by_kinds(deal)
    company = deal.company
    return [] unless company

    counteragents = company.counteragents.asc_by_name

    # group conteragents by kind
    grouped_counteragents = counteragents.group_by &:kind

    # build options
    options = []
    grouped_counteragents.each do |kind, arr|
      options << [ kind.to_s, arr.map { |ct| [ct.to_s, ct.id]}]
    end
    options
  end

  def companies_deals_types_collection
    [[I18n.t('.activerecord.attributes.company/deal.main'), true], [I18n.t('.activerecord.attributes.company/deal.usual'), false]]
  end
end
