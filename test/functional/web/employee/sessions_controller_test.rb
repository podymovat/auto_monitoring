require 'test_helper'

class Web::Employee::SessionsControllerTest < ActionController::TestCase

  test "should authenticate" do
    attrs = {}
    employee = create(:employee)
    attrs[:email] = employee.email
    attrs[:password] = employee.password
    post :create, :employee => attrs
    assert_response :redirect
    assert employee_signed_in?
  end

  test "should not authenticate" do
    attrs = {}
    employee = create(:employee)
    attrs[:email] = employee.email
    attrs[:password] = 'wrong_password'
    post :create, :employee => attrs
    assert_response :success
    assert !employee_signed_in?
  end

  test "should get new" do
    get :new
    assert_response :success
  end

  test "should delete destroy" do
    employee = create(:employee)
    sign_in(employee)
    delete :destroy
    assert_response :redirect
    assert !employee_signed_in?
  end
end
