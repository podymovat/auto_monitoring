class ContactDetailKindValueSourceInput < SimpleForm::Inputs::StringInput

  def input_html_options
    super.merge(rel: @builder.object.kind_id? ? @builder.object.kind.slug.to_s : '')
  end

  def input_html_classes
    super.push('contact_detail')
  end
end
