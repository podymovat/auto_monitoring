class Employee::PositionPrivilege < ActiveRecord::Base
  attr_accessible :position_id, :privilege_id

  belongs_to :position
  belongs_to :privilege
end
