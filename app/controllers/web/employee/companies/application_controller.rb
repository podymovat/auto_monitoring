class Web::Employee::Companies::ApplicationController < Web::Employee::ProtectedApplicationController
  before_filter do
    add_i18n_breadcrumb :general, :employee_companies_path
    add_breadcrumb current_company, employee_company_path(current_company)
  end

  def current_company
    scope = build_scope 'company'
    scope.get(params[:company_id])
  end
end
