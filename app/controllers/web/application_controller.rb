class Web::ApplicationController < ApplicationController
  include BreadcrumbsHelper

  respond_to :html
  helper_method :title

  before_filter do
    gon.license_fee_id = ::Company::Invoice::Kind::LICENSE_FEE_ID
  end

  protected

  def redirect_to_from_params_or(default_path)
    redirect_to (params[:redirect_to] || default_path)
  end

  private

  def base_title
    title t("title.base")
  end

  def title(part = nil)
    @parts ||= []
    unless part
      return nil if @parts.blank?
      return @parts.reverse.join(' - ')
    end
    @parts << part
  end
end
