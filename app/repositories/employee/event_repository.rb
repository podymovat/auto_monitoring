module Employee::EventRepository
  extend ActiveSupport::Concern
  include UsefullScopes

  included do
    search_methods :employee_event_search

    scope :employee_event_search, lambda {|scope| send(scope)}
    scope :expired, lambda { where('due_date < ?', Date.today) }
    scope :today, lambda { where(:due_date => Date.today) }
    scope :this_week, lambda { where(:due_date => Date.today.monday..Date.today.sunday) }
    scope :upcoming, lambda { where('due_date > ?', Date.today) }

    scope :unassigned, where(:company_person_id => nil)
    scope :company_assigned_to, lambda { |value| joins(:company).where(:companies => {:assigned_id => value})}
  end
end
