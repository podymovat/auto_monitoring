$ = jQuery
$.fn.extend
  search: (options) ->
    settings =
      minLength: 3

    settings = $.extend settings, options
    return @each ()->
      $('#term', this).autocomplete
        minLength: settings.minLength,
        source: Routes.api_search_index_path({format: 'json'}),
        select: (event, ui) ->
          window.location.href = ui.item.href

$ ->
  $('#search').search()