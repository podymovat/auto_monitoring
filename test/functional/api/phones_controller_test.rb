require 'test_helper'

class Api::PhonesControllerTest < ActionController::TestCase

  def setup
    @employee = create(:employee)
    sign_in(@employee)
    @company = create(:company, :creator => @employee)
    @company_person = create('company/person', :company => @company)
    @params = { :format => :json}
  end

  test "should get exists"do
    attrs = { :q => @company.contact_details.phones.last }
    get :exists, @params.merge(attrs)
    assert_response :success
  end
end
