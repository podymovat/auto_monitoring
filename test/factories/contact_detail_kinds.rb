FactoryGirl.define do
  factory 'contact_detail/kind' do
    name
    slug
  end
end
