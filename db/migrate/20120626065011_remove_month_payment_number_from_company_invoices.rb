class RemoveMonthPaymentNumberFromCompanyInvoices < ActiveRecord::Migration
  def change
    remove_column :company_invoices, :month_payment_number
  end
end
