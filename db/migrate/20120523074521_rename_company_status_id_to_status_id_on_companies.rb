class RenameCompanyStatusIdToStatusIdOnCompanies < ActiveRecord::Migration
  def change
    rename_column :companies, :company_status_id, :status_id
  end
end
