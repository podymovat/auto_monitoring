class Web::Employee::CompaniesPeople::ApplicationController < Web::Employee::ProtectedApplicationController
  protected

  def current_company_person
    scope = build_scope 'company/person'
    scope.get(params[:companies_person_id])
  end
end
