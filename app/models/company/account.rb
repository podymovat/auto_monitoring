class Company::Account < ActiveRecord::Base
  include AccountRepository

  attr_accessible :login, :company_id, :type

  belongs_to :company
  belongs_to :creator, :polymorphic => true

  has_many :accounts, :as => :creator

  validates :login, :presence => true, :length => {:maximum => 255},
            :uniqueness => {:case_sensitive => false}, :slug => true
  validates :company, :presence => true
  validates :creator, :presence => true

  state_machine :state, :initial => :active do
    state :active
  end

  def to_s
    login
  end
end
