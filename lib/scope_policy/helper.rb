module ScopePolicy::Helper

  def build_scope(model_name, options = {})
    options = options.merge(employee_constraints)
    ScopePolicy::Factory.build(model_name, current_employee, options)
  end
end
