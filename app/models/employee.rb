class Employee < ActiveRecord::Base
  include EmployeeRepository
  has_secure_password

  attr_accessible :email, :password, :first_name, :last_name, :patronymic, :position_id, :privilege_ids

  belongs_to :position

  has_many :events, :dependent => :destroy
  has_many :company_person_notes, :class_name => 'Company::Person::Note', :dependent => :destroy
  has_many :company_invoice_notes, :class_name => 'Company::Invoice::Note', :dependent => :destroy
  has_many :company_notes, :class_name => 'Company::Note', :dependent => :destroy
  has_many :employee_credentials, :dependent => :destroy
  has_many :privileges, :through => :employee_credentials
  has_many :accounts, :as => :creator

  validates :email, :presence => true, :email => true, :uniqueness => {:case_sensitive => false}
  validates :first_name, :presence => true, :length => {:maximum => 256}
  validates :last_name, :presence => true, :length => {:maximum => 256}
  validates :patronymic, :length => {:maximum => 256}

  state_machine :state, :initial => :active do
    state :active
    state :deleted

    event :activate do
      transition any - :active => :active
    end

    event :del do
      transition any - :deleted => :deleted
    end
  end

  def can?(name)
    privilege = Privilege.find_by_key(name)
    if privilege
      privileges.include?(privilege)
    else
      raise Privilege::NotFoundError, "Privilege with name #{name} not found."
    end
  end

  def full_name
    "#{first_name} #{last_name}"
  end

  def to_s
    full_name
  end
end
