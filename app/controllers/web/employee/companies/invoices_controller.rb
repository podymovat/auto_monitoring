class Web::Employee::Companies::InvoicesController < Web::Employee::Companies::ApplicationController
  def show
    scope = build_scope 'company/invoice'
    @company_invoice = scope.get(params[:id]).decorate
    @invoice_product_items = Company::InvoiceProductItemDecorator.decorate(@company_invoice.invoice_product_items)
    @notes = @company_invoice.notes
    @note = current_employee.company_invoice_notes.build

    add_i18n_breadcrumb :index, employee_companies_invoices_path({:search => {:company_id_equals => current_company.id}})
    add_breadcrumb t('breadcrumbs.web.employee.companies.invoices.show', {:number => @company_invoice.invoice_number}), employee_company_invoice_path(@company_invoice.company, @company_invoice)
  end

  def trigger_state_event
    scope = build_scope 'company/invoice'
    @company_invoice = scope.get(params[:id])
    @company_invoice.fire_state_event(params[:event])

    redirect_to_from_params_or(employee_companies_invoices_path)
  end
end
