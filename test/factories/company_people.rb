FactoryGirl.define do
  factory 'company/person' do
    name
    company
    creator
    position

    after(:create) do |p|
      FactoryGirl.create 'company/person/note', person: p
      FactoryGirl.create 'contact_detail', contactable: p
    end
  end
end
