class Web::Employee::CompaniesInvoicesController < Web::Employee::ProtectedApplicationController
  add_i18n_breadcrumb :general, :employee_companies_path

  def index
    title t(:companies_invoices)

    params[:search] = {:meta_sort => 'id.asc'}.merge(params[:search] || {})
    scope = build_scope 'company/invoice'
    @search = scope.invoices_list.metasearch(params[:search])
    @companies_invoices = Company::InvoiceDecorator.decorate(@search.page(params[:page]).per(params[:per_page]))

    add_i18n_breadcrumb :index, :employee_companies_invoices_path
  end

  def new
    @company_invoice = NewCompanyInvoiceType.new(params[:company_invoice])
    @company_invoice[:kind_id] = ::Company::Invoice::Kind::USUAL_ID
    @company_deals = company_deals

    add_i18n_breadcrumb :index, :employee_companies_invoices_path
    add_i18n_breadcrumb :new, :new_employee_companies_invoice_path
  end

  def create
    @company_invoice = NewCompanyInvoiceType.new(params[:company_invoice])
    @company_invoice.creator = current_employee

    if @company_invoice.save
      flash[:success] = flash_translate(:success)
      redirect_to employee_company_path(@company_invoice.company)
    else
      @company_deals = company_deals

      add_i18n_breadcrumb :index, :employee_companies_invoices_path
      add_i18n_breadcrumb :new, :new_employee_companies_invoice_path
      render :action => :new
    end
  end

  def edit
    scope = build_scope 'company/invoice'
    @company_invoice = scope.get(params[:id])
    old_type = @company_invoice.type.dup
    amount = @company_invoice.amount
    @company_invoice = @company_invoice.becomes(NewCompanyInvoiceType)
    @company_invoice.old_type = old_type
    @company_invoice.amount = amount

    @company_deals = @company_invoice.company.deals.asc_by_deal_number

    add_breadcrumb @company_invoice.company, employee_company_path(@company_invoice.company)
    add_i18n_breadcrumb :index, :employee_companies_invoices_path
    add_breadcrumb t('breadcrumbs.web.employee.companies.invoices.edit', {:number => @company_invoice.invoice_number}), :edit_employee_companies_invoice_path
  end

  def update
    scope = build_scope 'company/invoice'
    @company_invoice = scope.get(params[:id])
    @company_invoice = @company_invoice.becomes(NewCompanyInvoiceType)

    if @company_invoice.update_attributes(params[:company_invoice])
      flash[:success] = flash_translate(:success)
      redirect_to employee_company_path(@company_invoice.company)
    else
      @company_deals = @company_invoice.company.deals.asc_by_deal_number

      add_breadcrumb @company_invoice.company, employee_company_path(@company_invoice.company)
      add_i18n_breadcrumb :index, :employee_companies_invoices_path
      add_breadcrumb t('breadcrumbs.web.employee.companies.invoices.edit', {:number => @company_invoice.invoice_number}), :edit_employee_companies_invoice_path
      render :action => :edit
    end
  end

  def destroy
    scope = build_scope 'company/invoice'
    @company_invoice = scope.get(params[:id])
    @company_invoice.destroy

    redirect_to :action => :index
  end

  protected

  def company_deals
    if params[:company_invoice] && params[:company_invoice][:company_id]
      ::Company::Deal.asc_by_deal_number.find_all_by_company_id(params[:company_invoice][:company_id])
    else
      []
    end
  end
end
