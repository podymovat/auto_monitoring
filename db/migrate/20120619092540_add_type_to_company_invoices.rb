class AddTypeToCompanyInvoices < ActiveRecord::Migration
  def change
    change_table :company_invoices do |t|
      t.string :type
    end
  end
end
