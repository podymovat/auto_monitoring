class RenameCompanyPersonIdToPersonIdOnCompanyPersonNotes < ActiveRecord::Migration
  def change
    rename_column :company_person_notes, :company_person_id, :person_id
  end
end
