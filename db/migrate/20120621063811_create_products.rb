class CreateProducts < ActiveRecord::Migration
  def change
    create_table :products do |t|
      t.string :name
      t.integer :prime_amount_in_kop
      t.integer :sail_amount_in_kop
      t.string :currency
      t.string :state
      t.references :employee

      t.timestamps
    end
  end
end
