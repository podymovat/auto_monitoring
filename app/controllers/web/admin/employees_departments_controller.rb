class Web::Admin::EmployeesDepartmentsController < Web::Admin::ProtectedApplicationController
  add_i18n_breadcrumb :index, :admin_employees_departments_path

  def index
    title t(:employees_departments)

    params[:search] = {:meta_sort => 'created_at.asc'}.merge(params[:search] || {})
    @search = ::Employee::Department.metasearch(params[:search])
    @employees_departments = @search.page(params[:page]).per(params[:per_page])
  end

  def new
    @employee_department = ::Employee::Department.new

    add_i18n_breadcrumb :new, :new_admin_employees_department_path
  end

  def edit
    @employee_department = ::Employee::Department.find(params[:id])

    add_breadcrumb @employee_department, :edit_admin_employees_department_path
  end

  def create
    @employee_department = ::Employee::Department.new(params[:employee_department])

    if @employee_department.save
      flash[:success] = flash_translate(:success)
      redirect_to :action => :index
    else
      add_i18n_breadcrumb :new, :new_admin_employees_department_path
      render :action => :new
    end
  end

  def update
    @employee_department = ::Employee::Department.find(params[:id])

    add_breadcrumb @employee_department, :edit_admin_employees_department_path

    if @employee_department.update_attributes(params[:employee_department])
      flash[:success] = flash_translate(:success)
      redirect_to :action => :index
    else
      render :action => :edit
    end
  end

  def destroy
    @employee_department = ::Employee::Department.find(params[:id])
    @employee_department.destroy

    redirect_to :action => :index
  end
end
