class CreateCompanyInvoiceKinds < ActiveRecord::Migration
  def change
    create_table :company_invoice_kinds do |t|
      t.string :name
    end
  end
end
