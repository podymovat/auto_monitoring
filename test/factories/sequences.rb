FactoryGirl.define do
  sequence :integer, :aliases => [:cost, :cars_count, :product_items_count] do |n|
    n
  end

  sequence :amount, :aliases => [:total_amount, :paid_amount, :rental_fee, :prime_amount, :sail_amount, :equity_amount] do |n|
    n
  end

  sequence :password, :aliases => [:password_text] do |n|
    "password-#{n}"
  end

  sequence :body, :aliases => [:description, :kind_value_source, :value] do |n|
    "body-#{n}"
  end

  sequence :name, :aliases => [:login, :first_name, :last_name, :patronymic, :deal_number, :invoice_number, :abbreviation, :position] do |n|
    "name-#{n}"
  end

  sequence :slug, :aliases => [:key] do |n|
    "slug-#{n}"
  end

  sequence :email do |n|
    "email_#{n}@mail.com"
  end

  sequence :phone do |n|
    country_code = 8
    zero_sequence = '0' * (9 - n.to_s.length)
    "#{country_code}#{n}#{zero_sequence}"
  end

  sequence :color do |n|
    "%06d" % n
  end
  sequence :date, :aliases => [:deal_date, :due_date] do
    Time.now
  end
end
