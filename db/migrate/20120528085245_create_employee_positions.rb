class CreateEmployeePositions < ActiveRecord::Migration
  def change
    create_table :employee_positions do |t|
      t.string :name
      t.string :state
      t.references :department

      t.timestamps
    end
  end
end
