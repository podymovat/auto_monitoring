# encoding: UTF-8

class Company::Invoice < ActiveRecord::Base
  include InvoiceRepository

  INVOICE_NUMBER_SEQUENCE = 'company_invoices_invoice_number_seq'

  attr_accessible :amount, :company_id, :kind_id, :invoice_number, :deal_id,
                  :payment_state_event, :expected_payment_date,
                  :actual_payment_date, :documents_returned, :state_event,
                  :type, :equity_amount,
                  :invoice_product_items_attributes, :payment_method_id

  after_initialize :init

  before_create :generate_invoice_number
  before_save :record_payment_state_cache

  belongs_to :company
  belongs_to :creator, :class_name => ::Employee, :foreign_key => 'employee_id'
  belongs_to :kind
  belongs_to :deal
  belongs_to :payment_method

  composed_of :amount, ComposeOfParams.money(:amount_in_kop)
  composed_of :equity_amount, ComposeOfParams.money(:equity_amount_in_kop)

  has_many :notes, :dependent => :destroy, :order => 'created_at DESC'
  has_many :invoice_product_items, :dependent => :destroy
  has_many :product_items, :through => :invoice_product_items

  validates :company, :presence => true
  validates :creator, :presence => true
  validates :kind, :presence => true
  validates :amount, :presence => true, :numericality => { :greater_than_or_equal_to => 0  }
  validates :equity_amount, :presence => true, :numericality => { :greater_than_or_equal_to => 0  }
  validates :invoice_number, :length => {:maximum => 255}
  validates :deal, :presence => true
  validates :type, :presence => true, :length => {:maximum => 255}

  accepts_nested_attributes_for :invoice_product_items, :allow_destroy => true

  state_machine :payment_state, :initial => :unpaid do
    state :paid do
      validates :amount, :presence => true, :numericality => { :greater_than => 0  }
      validates :actual_payment_date, :presence => true
    end
    state :unpaid

    event :pay do
      transition :unpaid => :paid
    end
  end

  state_machine :state, :initial => :active do
    state :active
    state :archived
    state :deleted

    event :activate do
      transition any - :active => :active
    end

    event :archive do
      transition :active => :archived
    end

    event :del do
      transition any - :deleted => :deleted
    end
  end

  def counteragent
    deal_id? ? deal.counteragent : nil
  end

  def with_cash?
    payment_method == PaymentMethod.cash
  end

  def to_s
    invoice_number
  end

  private

  def init
    if new_record?
      self.amount ||= 0.0
    end
  end

  def record_payment_state_cache
    self.payment_state_cache = human_payment_state_name
  end

  def generate_invoice_number
    number = deal.juridical_person.next_invoice_number
    deal.juridical_person.save

    self.invoice_number ||= "%s_%04d" %
      [deal.juridical_person.invoice_number_prefix, number]
  end
end
