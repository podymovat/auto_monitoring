class AddUniqueIndexOnNameToCompanyCounteragentKinds < ActiveRecord::Migration
  def change
    add_index :company_counteragent_kinds, :name, :unique => true
  end
end
