module Web::Admin::EmployeesDepartmentsHelper

  def departments
    ::Employee::Department.all
  end
end
