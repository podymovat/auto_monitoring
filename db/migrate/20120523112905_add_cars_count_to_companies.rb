class AddCarsCountToCompanies < ActiveRecord::Migration
  def change
    add_column :companies, :cars_count, :integer
  end
end
