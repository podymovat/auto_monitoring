FactoryGirl.define do
  factory 'company/category', :aliases => [:company_category] do
    name
  end
end
