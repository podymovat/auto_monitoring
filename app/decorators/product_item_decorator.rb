class ProductItemDecorator < ApplicationDecorator
  decorates :product_item

  def prime_amount
    source.prime_amount.format
  end

  def sail_amount
    source.sail_amount.format
  end
end