class Web::Company::InvoicesController < Web::Company::ProtectedApplicationController
  add_i18n_breadcrumb :general, :company_invoices_path

  def index
    title t(:companies_invoices)

    params[:search] = {:meta_sort => 'id.asc'}.merge(params[:search] || {})
    @search = current_company.invoices.metasearch(params[:search])
    @invoices = Company::InvoiceDecorator.decorate(@search.page(params[:page]).per(params[:per_page]))
  end

  def show
    @invoice = current_company.invoices.find(params[:id]).decorate
    @invoice_product_items = Company::InvoiceProductItemDecorator.decorate(@invoice.invoice_product_items)

    add_breadcrumb t('breadcrumbs.web.employee.companies.invoices.show', {:number => @invoice.invoice_number}), company_invoice_path(@invoice)
  end
end
