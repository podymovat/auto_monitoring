set :user, 'auto_monitoring_production'
set :rails_env, 'production'

role :db,  "62.76.184.142", :primary => true # This is where Rails migrations will run
role :app, "62.76.184.142"
role :web, "62.76.184.142"

set :branch, "master"
set :deploy_to, "/u/apps/auto_monitoring"
