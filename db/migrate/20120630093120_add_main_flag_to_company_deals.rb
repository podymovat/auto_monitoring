class AddMainFlagToCompanyDeals < ActiveRecord::Migration
  def change
    add_column :company_deals, :main_flag, :boolean
  end
end
