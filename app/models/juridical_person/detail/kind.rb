class JuridicalPerson::Detail::Kind < ActiveRecord::Base
  include UsefullScopes

  attr_accessible :name

  validates :name, :presence => true, :uniqueness => {:case_sensitive => false},
            :length => {:maximum => 255}

  def to_s
    name
  end
end
