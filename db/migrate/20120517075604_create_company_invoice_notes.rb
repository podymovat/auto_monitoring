class CreateCompanyInvoiceNotes < ActiveRecord::Migration
  def change
    create_table :company_invoice_notes do |t|
      t.text :body
      t.references :employee
      t.references :invoice
      t.string :state

      t.timestamps
    end
  end
end
