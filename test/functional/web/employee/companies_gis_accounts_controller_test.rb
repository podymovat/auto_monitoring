require 'test_helper'

class Web::Employee::CompaniesGisAccountsControllerTest < ActionController::TestCase
  def setup
    @employee = create(:employee)
    sign_in(@employee)
    @company = create(:company, :creator => @employee, :assigned => @employee)
    @company_person = create('company/person', :creator => @employee, :company => @company)
    @company_gis_account = create('company/gis_account', :company => @company)
    @params = {:id => @company_gis_account.id}
  end

  test "should get new" do
    get :new
    assert_response :success
  end

  test "should post create" do
    attrs = attributes_for 'company/gis_account', :company_id => @company.id, :type => ::Company::GisAccount

    post :create, :company_gis_account => attrs
    assert_response :redirect

    company_gis_account = ::Company::GisAccount.last
    assert_equal attrs[:login], company_gis_account.login
  end

  test "should get edit" do
    get :edit, @params
    assert_response :success
  end

  test "should put update" do
    attrs = {}
    attrs[:password_text]= generate :password
    attrs[:login] = generate :login
    put :update, @params.merge(:company_gis_account => attrs)
    assert_response :redirect

    @company_gis_account.reload
    assert_equal attrs[:login], @company_gis_account.login
  end
end
