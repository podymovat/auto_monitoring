class AddEquityAmountToCompanyInvoice < ActiveRecord::Migration
  def change
    add_column :company_invoices, :equity_amount_in_kop, :integer
  end
end
