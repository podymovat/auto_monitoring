class Web::Employee::CompaniesDealsController < Web::Employee::ProtectedApplicationController
  add_i18n_breadcrumb :general, :employee_companies_path

  def index
    title t(:companies_deals)

    params[:search] = {:meta_sort => 'id.asc'}.merge(params[:search] || {})
    scope = build_scope 'company/deal'
    @search = scope.deals_list.metasearch(params[:search])
    @companies_deals = Company::DealDecorator.decorate(@search.page(params[:page]).per(params[:per_page]))

    add_i18n_breadcrumb :index, :employee_companies_deals_path
  end

  def new
    @company_deal = ::Company::Deal.new(params[:company_deal])
    @company_deal.deal_date = Date.today

    add_i18n_breadcrumb :index, :employee_companies_deals_path
    add_i18n_breadcrumb :new, :new_employee_companies_deal_path
  end

  def create
    @company_deal = ::Company::Deal.new(params[:company_deal])
    @company_deal.creator = current_employee

    if @company_deal.save
      flash[:success] = flash_translate(:success)
      redirect_to employee_company_path(@company_deal.company)
    else
      add_i18n_breadcrumb :index, :employee_companies_deals_path
      add_i18n_breadcrumb :new, :new_employee_companies_deal_path
      render :action => :new
    end
  end

  def edit
    scope = build_scope 'company/deal'
    @company_deal = scope.get(params[:id])

    add_breadcrumb @company_deal.company, employee_company_path(@company_deal.company)
    add_i18n_breadcrumb :index, :employee_companies_deals_path
    add_breadcrumb @company_deal.deal_number, :edit_employee_companies_deal_path
  end

  def update
    scope = build_scope 'company/deal'
    @company_deal = scope.get(params[:id])

    if @company_deal.update_attributes(params[:company_deal])
      flash[:success] = flash_translate(:success)
      redirect_to employee_company_path(@company_deal.company)
    else
      add_breadcrumb @company_deal.company, employee_company_path(@company_deal.company)
      add_i18n_breadcrumb :index, :employee_companies_deals_path
      add_breadcrumb @company_deal.deal_number, :edit_employee_companies_deal_path
      render :action => :edit
    end
  end

  def destroy
    scope = build_scope 'company/deal'
    @company_deal = scope.get(params[:id])
    @company_deal.destroy

    redirect_to :action => :index
  end
end
