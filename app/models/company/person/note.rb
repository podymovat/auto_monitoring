class Company::Person::Note < ActiveRecord::Base
  attr_accessible :body, :person_id

  belongs_to :person
  belongs_to :creator, :class_name => ::Employee, :foreign_key => 'employee_id'

  validates :person, :presence => true
  validates :creator, :presence => true
  validates :body, :presence => true

  state_machine :state, :initial => :published do
    state :published
  end
end
