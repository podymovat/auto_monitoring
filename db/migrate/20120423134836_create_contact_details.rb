class CreateContactDetails < ActiveRecord::Migration
  def change
    create_table :contact_details do |t|
      t.integer :kind_id
      t.string :value
      t.references :contactable, :polymorphic => true
    end

    add_index :contact_details, :kind_id
  end
end
