class Company::Counteragent < ActiveRecord::Base
  include UsefullScopes

  attr_accessible :name, :kind_id, :company_id, :cars_count

  belongs_to :kind
  belongs_to :company

  validates :name, :presence => true,
            :uniqueness => { :scope => [:company_id, :kind_id] },
            :length => {:maximum => 256}
  validates :kind, :presence => true
  validates :cars_count,
            :numericality => { :only_integer => true, :greater_than_or_equal_to => 0 },
            :allow_nil => true

  state_machine :state, :initial => :active do
    state :active
  end

  def to_s
    "#{kind} | #{name}"
  end
end
