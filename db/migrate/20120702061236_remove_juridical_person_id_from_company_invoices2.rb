class RemoveJuridicalPersonIdFromCompanyInvoices2 < ActiveRecord::Migration
  def change
    remove_column :company_invoices, :juridical_person_id
  end
end
