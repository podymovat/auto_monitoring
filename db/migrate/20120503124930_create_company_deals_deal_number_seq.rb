class CreateCompanyDealsDealNumberSeq < ActiveRecord::Migration
  def up
    execute "CREATE SEQUENCE company_deals_deal_number_seq MAXVALUE 999 CYCLE"
  end

  def down
    execute "DROP SEQUENCE company_deals_deal_number_seq"
  end
end
