class Web::Employee::CompaniesInvoices::NotesController < Web::Employee::CompaniesInvoices::ApplicationController
  def create
    @invoice = current_company_invoice
    @note = @invoice.notes.build(params[:company_invoice_note])
    @note.creator = current_employee

    if @note.save
      flash[:success] = flash_translate(:success)
    else
      flash[:error] = flash_translate(:error)
    end
    redirect_to employee_company_invoice_path(@invoice.company, @invoice)
  end
end
