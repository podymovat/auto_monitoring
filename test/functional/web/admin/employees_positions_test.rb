require 'test_helper'

class Web::Admin::EmployeesPositionsControllerTest < ActionController::TestCase

  def setup
    @signed_in_employee = create(:employee)
    sign_in(@signed_in_employee)
    @employee_position = create('employee/position')
    @employee_privilege = create('employee/privilege')
    @params = {:id => @employee_position.id}
  end

  test "should get index" do
    get :index
    assert_response :success
  end

  test "should get new" do
    get :new
    assert_response :success
  end

  test "should get edit" do
    get :edit, @params
    assert_response :success
  end

  test "should post create" do
    attrs = attributes_for 'employee/position'
    attrs[:privilege_ids] = @employee_privilege.id

    post :create, :employee_position => attrs
    assert_response :redirect

    employee_position = ::Employee::Position.last
    assert employee_position
  end

  test "should put update" do
    name = @employee_position.name

    attrs = {}
    attrs[:name] = generate :name
    put :update, @params.merge(:employee_position => attrs)
    assert_response :redirect

    @employee_position.reload
    assert @employee_position.name != name
  end

  test "should delete destroy" do
    delete :destroy, @params
    assert_response :redirect

    assert !Employee::Position.exists?(@employee_position.id)
  end
end
