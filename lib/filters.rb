require 'addressable/uri'

module Filters

  def self.phone(value)
    # remove all non-digits
    clean_value = value.gsub(/\D/, '')

    country_codes = configus.phone.country_codes
    area_code = configus.phone.defaults.area_code

    case clean_value.length
      when 7
        "#{area_code}#{clean_value}"
      when 11
        # remove country code only if phone starts with the allowed country code
        if country_codes.include?(clean_value[0].to_i)
          clean_value[1..-1]
        else
          clean_value
        end
      else clean_value
    end
  end

  def self.site(value)
    uri = Addressable::URI.parse(value)
    uri.scheme = "http" if uri.scheme.blank?
    host = uri.host.sub(/\www\./, '') if uri.host.present?
    path = (uri.path.present? && uri.host.blank?) ? uri.path.sub(/\www\./, '') : uri.path
    canonical_url = uri.scheme.to_s + "://" + host.to_s + path.to_s
  rescue Addressable::URI::InvalidURIError
    nil
  rescue URI::Error
    nil
  end
end
