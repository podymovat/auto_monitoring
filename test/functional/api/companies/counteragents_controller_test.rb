require 'test_helper'

class Api::Companies::CounteragentsControllerTest < ActionController::TestCase

  def setup
    @employee = create(:employee)
    sign_in(@employee)
    @company = create(:company, :creator => @employee)
    @params = { :format => :json }
  end

  test "should get index" do
    attrs = { :company_id => @company.id }
    get :index, @params.merge(attrs)
    assert_response :success
  end
end
