FactoryGirl.define do
  factory 'employee/privilege' do
    name
    key
    association :kind, :factory => 'employee/privilege/kind'
  end
end
