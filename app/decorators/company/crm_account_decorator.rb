class Company::CrmAccountDecorator < Company::AccountDecorator
  decorates 'company/crm_account'

  def account_type
    I18n.t('.activerecord.attributes.company/account.crm_account')
  end
end
