class AddUniqueIndexOnNameToCompanyInvoiceKinds < ActiveRecord::Migration
  def change
    add_index :company_invoice_kinds, :name, :unique => true
  end
end
