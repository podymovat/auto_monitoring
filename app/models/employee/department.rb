class Employee::Department < ActiveRecord::Base
  attr_accessible :name, :positions_attributes

  has_many :employees
  has_many :positions, :dependent => :destroy

  validates :name, :presence => true, :length => {:maximum => 256}

  accepts_nested_attributes_for :positions, :reject_if => :all_blank, :allow_destroy => true

  state_machine :state, :initial => :active do
    state :active
  end

  def to_s
    name
  end
end
