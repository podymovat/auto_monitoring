class AddEmployeeToCompanyAccount < ActiveRecord::Migration
  def change
    change_table :company_accounts do |t|
      t.references :employee
    end
  end
end
