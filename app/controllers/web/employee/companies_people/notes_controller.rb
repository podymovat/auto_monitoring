class Web::Employee::CompaniesPeople::NotesController < Web::Employee::CompaniesPeople::ApplicationController
  def create
    @person = current_company_person
    @note = @person.notes.build(params[:company_person_note])
    @note.creator = current_employee

    if @note.save
      flash[:success] = flash_translate(:success)
    else
      flash[:error] = flash_translate(:error)
    end
    redirect_to employee_company_person_path(@person.company, @person)
  end
end
