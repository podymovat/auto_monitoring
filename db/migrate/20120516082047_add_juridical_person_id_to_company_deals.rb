class AddJuridicalPersonIdToCompanyDeals < ActiveRecord::Migration
  def change
    add_column :company_deals, :juridical_person_id, :integer
    add_index :company_deals, :juridical_person_id
  end
end
