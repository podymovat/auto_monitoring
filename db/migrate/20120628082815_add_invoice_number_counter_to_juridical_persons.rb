class AddInvoiceNumberCounterToJuridicalPersons < ActiveRecord::Migration
  def change
    add_column :juridical_people, :invoice_number_counter, :integer
  end
end
