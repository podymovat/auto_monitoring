$ = jQuery
$.fn.extend
  company_invoice: (options) ->
    settings =
      dateFormat:
        db: 'yy-mm-dd'
      license_fee_id: gon.license_fee_id

    settings = $.extend settings, options
    return @each ()->
      $('#company_invoice_expected_payment_date, #company_invoice_actual_payment_date, #company_invoice_period_start_date, #company_invoice_period_end_date', this).datepicker
        numberOfMonths: 2,
        showButtonPanel: true,
        dateFormat: settings.dateFormat.db

      company = $('#company_invoice_company_id')
      deal = $('#company_invoice_deal_id')

      # disable deal/person select on startup
      if !company.val()
        deal.attr('disabled', 'disabled')

      company.change (event, callback) ->
        if this.value
          deal.load(
            Routes.api_company_deals_path(this.value),
            ->
              $(this).removeAttr('disabled')
          )
        else
          deal.val($('options:first', deal).val())
          deal.attr('disabled', 'disabled')

      kind = $('#company_invoice_kind_id')
      period_start_date = $('#company_invoice_period_start_date')
      period_end_date = $('#company_invoice_period_end_date')
      period_start_date_wrap = period_start_date.closest('.control-group')
      period_end_date_wrap = period_end_date.closest('.control-group')

      current_kind = settings.license_fee_id
      if parseInt(kind.val()) != current_kind
        period_start_date_wrap.hide()
        period_end_date_wrap.hide()

      kind.change (event, callback) ->
        if parseInt(this.value) == current_kind
          period_start_date_wrap.show()
          period_end_date_wrap.show()
        else
          period_start_date_wrap.hide()
          period_end_date_wrap.hide()


$ ->
  $(document).company_invoice()
