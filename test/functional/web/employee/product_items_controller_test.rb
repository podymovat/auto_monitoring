require 'test_helper'

class Web::Employee::ProductItemsControllerTest < ActionController::TestCase
  def setup
    @employee = create(:employee)
    sign_in(@employee)
    @employee_privilege = Employee::Privilege.find_by_key(:product_items_manage)
    @employee_credential = create(:employee_credential, :employee => @employee, :privilege => @employee_privilege)
    @product_item = create(:product_item, :creator => @employee)
    @params = {:id => @product_item.id}
  end

  test "should get index" do
    get :index
    assert_response :success
  end

  test "should get new" do
    get :new
    assert_response :success
  end

  test "should post create" do
    attrs = attributes_for :product_item

    post :create, :product_item => attrs
    assert_response :redirect

    product_item = ProductItem.last
    assert_equal attrs[:name], product_item.name
  end

  test "should get edit" do
    get :edit, @params
    assert_response :success
  end

  test "should put update" do
    attrs = {}
    attrs[:name] = generate :name
    put :update, @params.merge(:product_item => attrs)
    assert_response :redirect

    product_item = ProductItem.find(@params[:id])
    assert_equal attrs[:name], product_item.name
  end

  test "should delete destroy" do
    delete :destroy, @params
    assert_response :redirect

    assert !ProductItem.exists?(@params[:id])
  end
end
