class Company::Note < ActiveRecord::Base
  attr_accessible :body, :company_id

  belongs_to :company
  belongs_to :creator, :class_name => ::Employee, :foreign_key => "employee_id"

  validates :company, :presence => true
  validates :creator, :presence => true
  validates :body, :presence => true

  state_machine :state, :initial => :published do
    state :published
  end
end
