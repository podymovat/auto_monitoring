class CreateCompanyPeople < ActiveRecord::Migration
  def change
    create_table :company_people do |t|
      t.string :first_name
      t.string :last_name
      t.string :patronymic
      t.integer :company_id

      t.timestamps
    end

    add_index :company_people, :company_id
  end
end
