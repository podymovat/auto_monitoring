FactoryGirl.define do
  factory :employee, :aliases => [:creator, :assigned]  do
    email
    password
    first_name
    last_name
    patronymic
    association :position, :factory => 'employee/position'
  end
end
