class CreateEmployeesEmployeePrivileges < ActiveRecord::Migration
  def change
    create_table :employees_privileges do |t|
      t.integer :employee_id
      t.integer :privilege_id
    end
  end
end
