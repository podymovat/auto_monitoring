require 'test_helper'

class Web::Employee::CompaniesDealsControllerTest < ActionController::TestCase

  def setup
    @employee = create(:employee)
    sign_in(@employee)
    @company = create(:company, :creator => @employee, :assigned => @employee)
    # pick random counteragent
    @counteragent = @company.counteragents.first
    @juridical_person = create(:juridical_person)
    @company_deal = create('company/deal', :company => @company)
    @params = {:id => @company_deal.id}
  end

  test "should get index" do
    get :index
    assert_response :success
  end

  test "should get new" do
    get :new
    assert_response :success
  end

  test "should post create" do
    attrs = attributes_for('company/deal', :company_id => @company.id, :counteragent_id => @counteragent.id, :juridical_person_id => @juridical_person.id)

    post :create, :company_deal => attrs
    assert_response :redirect

    company_deal = ::Company::Deal.last
    assert_equal attrs[:total_amount], company_deal.total_amount_in_kop / 100
  end

  test "should get edit" do
    get :edit, @params
    assert_response :success
  end

  test "should put update" do
    attrs = {}
    attrs[:total_amount] = generate(:integer)
    put :update, @params.merge(:company_deal => attrs)
    assert_response :redirect

    @company_deal.reload
    assert_equal attrs[:total_amount], @company_deal.total_amount_in_kop / 100
  end

  test "should delete destroy" do
    delete :destroy, @params
    assert_response :redirect

    assert !::Company::Deal.exists?(@params[:id])
  end
end
