class AddNameToCompanyPeople < ActiveRecord::Migration
  def change
    add_column :company_people, :name, :string
    remove_column :company_people, :first_name
    remove_column :company_people, :last_name
    remove_column :company_people, :patronymic
  end
end
