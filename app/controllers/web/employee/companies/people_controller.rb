class Web::Employee::Companies::PeopleController < Web::Employee::Companies::ApplicationController
  def show
    scope = build_scope 'company/person'
    @company_person = scope.get(params[:id])
    @notes = @company_person.notes
    @note = current_employee.company_person_notes.build

    add_breadcrumb @company_person, employee_company_person_path(@company_person.company, @company_person)
  end
end
