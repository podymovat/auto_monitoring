class PaymentStateEventInput < SimpleForm::Inputs::CollectionSelectInput
  def collection
    object.send("payment_state_transitions").map {|t| [t.human_event, t.event]}
  end
end