class AddAbbreviationToCompanyDealKinds < ActiveRecord::Migration
  def change
    add_column :company_deal_kinds, :abbreviation, :string
  end
end
