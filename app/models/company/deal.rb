# encoding: UTF-8

class Company::Deal < ActiveRecord::Base
  include DealRepository

  DEAL_NUMBER_SEQUENCE = 'company_deals_deal_number_seq'

  attr_accessible :company_id, :deal_date, :deal_number, :paid_amount, :total_amount, :counteragent_id,
                  :main_flag, :juridical_person_id

  composed_of :total_amount, ComposeOfParams.money(:total_amount_in_kop)
  composed_of :paid_amount, ComposeOfParams.money(:paid_amount_in_kop)

  belongs_to :company
  belongs_to :creator, :class_name => ::Employee, :foreign_key => 'employee_id'
  belongs_to :counteragent
  belongs_to :juridical_person

  has_many :invoices, :dependent => :destroy

  validates :company, :presence => true
  validates :total_amount, :numericality => { :greater_than_or_equal_to => 0 }
  validates :paid_amount, :numericality => { :greater_than_or_equal_to => 0 }
  validates :deal_number, :length => {:maximum => 256}
  validates :creator, :presence => true
  validates :counteragent, :presence => true
  validates :juridical_person, :presence => true

  after_initialize :init
  before_create :generate_deal_number

  state_machine :state, :initial => :active do
    state :active
  end

  def kind
    counteragent_id? ? counteragent.kind : nil
  end

  def to_s
    deal_number
  end

  def counteragent_id=(value)
    write_attribute(:counteragent_id, value)
    update_deal_number if persisted? && counteragent_id_changed?
  end

  private

  def init
    if new_record?
      self.total_amount ||= 0.0
      self.paid_amount ||= 0.0
    end
  end

  def generate_deal_number
    next_deal_number = connection.next_sequence_value(DEAL_NUMBER_SEQUENCE)
    self.deal_number ||= "К%03d-%s %s" % [next_deal_number, Time.zone.now.strftime('%y'), kind.abbreviation]
  end

  def update_deal_number
    deal_number_will_change!
    self.deal_number[-1] = kind.abbreviation
  end
end
