class CreateEmployeeEvents < ActiveRecord::Migration
  def change
    create_table :employee_events do |t|
      t.integer :employee_id
      t.text :description
      t.date :due_date
      t.string :state

      t.timestamps
    end

    add_index :employee_events, :employee_id
  end
end
