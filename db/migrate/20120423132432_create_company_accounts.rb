class CreateCompanyAccounts < ActiveRecord::Migration
  def change
    create_table :company_accounts do |t|
      t.string :login
      t.string :password_digest
      t.string :state
      t.references :company

      t.timestamps
    end
  end
end
