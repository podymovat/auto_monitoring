require 'test_helper'

class Web::Company::CrmAccountsControllerTest < ActionController::TestCase
  def setup
    @employee = create(:employee)
    sign_in(@employee)
    @company = create(:company, :creator => @employee, :assigned => @employee)
    @company_person = create('company/person', :creator => @employee, :company => @company)
    @company_crm_account = create('company/crm_account', :company => @company, :person => @company_person)
    employee_sign_out

    sign_in(@company_crm_account)
    @params = {:id => @company_crm_account.id}
  end

  test "should get edit" do
    get :edit, @params
    assert_response :success
  end

  test "should put update" do
    attrs = {}
    attrs[:password]= generate :password
    attrs[:login] = generate :login
    put :update, @params.merge(:company_crm_account => attrs)
    assert_response :redirect

    @company_crm_account.reload
    assert_equal attrs[:login], @company_crm_account.login
  end
end
