class AddJuridicalPersonIdToCompanyDeals2 < ActiveRecord::Migration
  def change
    change_table :company_deals do |t|
      t.references :juridical_person
    end
  end
end
