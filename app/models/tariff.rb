class Tariff < ActiveRecord::Base
  attr_accessible :cost, :name

  composed_of :cost, ComposeOfParams.money(:cost_in_kop)
  validates :name, :presence => true, :uniqueness => {:case_sensitive => false},
            :length => {:maximum => 255}
  validates :cost, :presence => true

  state_machine :state, :initial => :active do
    state :active
  end

  def to_s
    name
  end
end
