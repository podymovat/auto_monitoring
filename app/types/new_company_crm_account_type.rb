class NewCompanyCrmAccountType < Company::CrmAccount
  include BaseType

  validates :password, :presence => true

  before_save do
    self.type = Company::CrmAccount.name
  end
end
