class Employee::Position < ActiveRecord::Base
  include PositionRepository
  attr_accessible :name, :department_id, :privilege_ids

  belongs_to :department

  has_many :employees
  has_many :position_privileges, :dependent => :destroy
  has_many :privileges, :through => :position_privileges

  validates :name, :presence => true, :length => {:maximum => 256}

  state_machine :state, :initial => :active do
    state :active
  end

  def to_s
    name
  end
end
