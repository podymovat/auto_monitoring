AutoMonitoring::Application.routes.draw do

  namespace :api do
    resources :employees

    resources :companies do
      scope :module => :companies do
        resources :people, :only => [:index]
        resources :deals, :only => [:index]
        resources :counteragents, :only => [:index]
      end
    end

    resources :employee_positions, :only => [] do
      scope :module => :employee_positions do
        resources :privileges, :only => [:index]
      end
    end

    resources :product_items, :only =>[:show]

    resources :companies_tags, :only => [] do
      get :autocomplete, :on => :collection
    end

    resources :phones, :only => [] do
      get :exists, :on => :collection
    end

    resources :emails, :only => [] do
      get :exists, :on => :collection
    end

    resources :current_month_payment, :only => [] do
      get :month_number, :on => :collection
    end

    resources :search, :only => [:index]
  end

  scope :module => 'web' do
    root :to => 'welcome#index'

    namespace :admin do
      root :to => 'employees#index'
      resources :employees, :only => [:index, :show, :new, :create, :edit, :update] do
        put :trigger_state_event, :on => :member
      end
      resources :employees_departments
      resources :employees_positions
      resources :companies_categories
      resources :companies_statuses
      resources :juridical_people
    end

    namespace :employee do
      root :to => 'companies#index'

      resources :companies_people do
        scope :module => :companies_people do
          resources :notes, :only => [:create]
        end
      end

      resources :companies_invoices, :only => [:index, :new, :create, :edit, :update] do
        scope :module => :companies_invoices do
          resources :notes, :only => [:create]
        end
      end

      resources :companies do
        scope :module => :companies do
          resources :notes, :only => [:create]
          resources :people, :only => [:show]
          resources :invoices, :only => [:show] do
            put :trigger_state_event, :on => :member
          end
          resources :deals, :only => [:show]
        end
      end
      resources :companies_accounts, :only => [:index, :destroy]
      resources :companies_crm_accounts
      resources :companies_gis_accounts
      resources :companies_invoices
      resources :companies_deals
      resources :tariffs
      resources :events
      resources :product_items
      resource :session, :only => [:new, :create, :destroy]
      resources :search, :only => [:index]
    end

    namespace :company do
      root :to => 'deals#index'
      resources :deals, :only => [:index, :show]
      resources :invoices, :only => [:index, :show]
      resources :accounts
      resources :crm_accounts
      resources :gis_accounts
      resource :session, :only => [:new, :create, :destroy]
    end
  end
end
