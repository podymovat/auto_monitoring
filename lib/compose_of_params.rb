module ComposeOfParams
  def self.money(mapping_from)
    {
      :class_name => "Money",
      :mapping => [[mapping_from, 'cents'], %w(currency currency_as_string)],
      :constructor => Proc.new { |amount, _| Money.new(amount || 0, 'RUB') },
      :converter => Proc.new { |value|
        (value.is_a?(String) && (value.gsub(/\D/, '').blank? || value =~ /^.+-/)) ?
          NilMoney.new(value) :
          (value.respond_to?(:to_money) ?
            value.to_money('RUB') :
            raise(ArgumentError, "Can't convert #{value.class} to Money"))
      },
      :allow_nil => true
    }
  end
end