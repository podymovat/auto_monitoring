class ApplicationController < ActionController::Base
  include AuthHelper
  include EmployeeConstraintsHelper
  include ScopePolicy::Helper
  include FlashHelper
  protect_from_forgery

  before_filter :basic_auth if Rails.env.staging?
end
