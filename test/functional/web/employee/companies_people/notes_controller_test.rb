require 'test_helper'

class Web::Employee::CompaniesPeople::NotesControllerTest < ActionController::TestCase

  def setup
    @employee = create(:employee)
    sign_in(@employee)
    @company = create(:company, :creator => @employee, :assigned => @employee)
    @company_person = @company.people.last

    @params = {:companies_person_id => @company_person.id}
  end

  test "should post create" do
    attrs = attributes_for 'company/person/note'

    post :create, @params.merge(:company_person_note => attrs)
    assert_response :redirect

    note = Company::Person::Note.last
    assert_equal attrs[:body], note.body
  end
end
