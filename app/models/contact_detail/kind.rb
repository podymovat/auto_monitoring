class ContactDetail::Kind < ActiveRecord::Base
  include UsefullScopes

  attr_accessible :name, :slug

  validates :name, :presence => true, :length => {:maximum => 255}
  validates :slug, :slug => true, :uniqueness => true

  def to_s
    name
  end

  class << self

    def phone
      find_by_slug(:phone)
    end

    def email
      find_by_slug(:email)
    end

    def site
      find_by_slug(:site)
    end
  end
end
