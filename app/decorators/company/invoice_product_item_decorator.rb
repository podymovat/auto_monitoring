class Company::InvoiceProductItemDecorator < ApplicationDecorator
  decorates 'company/invoice_product_item'
  decorates_association :product_item

  def prime_amount
    source.prime_amount.format
  end

  def sail_amount
    source.sail_amount.format
  end
end
