FactoryGirl.define do
  factory 'company/person/note' do
    association :person, :factory => 'company/person'
    body
    creator
  end
end
