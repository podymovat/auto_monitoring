class AddPaymentStateCacheToComapnyInvoices < ActiveRecord::Migration
  def change
    add_column :company_invoices, :payment_state_cache, :string
  end
end
