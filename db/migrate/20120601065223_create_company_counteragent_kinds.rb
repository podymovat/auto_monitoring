class CreateCompanyCounteragentKinds < ActiveRecord::Migration
  def change
    create_table :company_counteragent_kinds do |t|
      t.string :name
      t.string :abbreviation
    end
  end
end
