class AddSlugToCompanyStatuses < ActiveRecord::Migration
  def change
    add_column :company_statuses, :slug, :string
  end
end
