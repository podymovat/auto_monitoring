class Web::Employee::CompaniesController < Web::Employee::ProtectedApplicationController
  add_i18n_breadcrumb :general, :employee_companies_path

  def index
    title t(:companies)

    params[:search] = {:meta_sort => 'id.asc'}.merge(params[:search] || {})
    scope = build_scope 'company'
    @search = scope.companies_list.metasearch(params[:search])
    @companies = CompanyDecorator.decorate(@search.page(params[:page]).per(params[:per_page]))
  end

  def new
    @company = ::Company.new
    @company.contact_details.build({ :kind_id => ContactDetail::Kind.phone.id })
    @company.contact_details.build({ :kind_id => ContactDetail::Kind.email.id })
    @company.assigned = current_employee

    add_i18n_breadcrumb :new, :new_employee_company_path
  end

  def show
    scope = build_scope 'company'
    @company = scope.get(params[:id]).decorate

    @notes = @company.notes
    @note = current_employee.company_notes.build
    @deals = Company::DealDecorator.decorate(@company.deals)
    @people = @company.people
    scope = build_scope 'company/invoice'
    invoices = Draper::DecoratedEnumerableProxy
      .new(scope.invoices_list.asc_by_created_at.find_all_by_company_id(@company.id),
           Company::Invoice)
      .decorated_collection
    @grouped_invoices = invoices.group_by &:deal_id

    add_breadcrumb @company, :employee_company_path
  end

  def create
    @company = ::Company.new(params[:company])
    @company.creator = current_employee

    if @company.save
      flash[:success] = flash_translate(:success)
      redirect_to :action => :show, :id => @company
    else
      add_i18n_breadcrumb :new, :new_employee_company_path
      render :action => :new
    end
  end

  def edit
    scope = build_scope 'company'
    @company = scope.get(params[:id])

    add_breadcrumb @company, :edit_employee_company_path
  end

  def update
    scope = build_scope 'company'
    @company = scope.get(params[:id])

    add_breadcrumb @company, :edit_employee_company_path

    if @company.update_attributes(params[:company])
      flash[:success] = flash_translate(:success)
      redirect_to :action => :show, :id => @company
    else
      render :action => :edit
    end
  end

  def destroy
    scope = build_scope 'company'

    if current_employee.can?(:company_delete)
      @company = scope.get(params[:id])
      @company.destroy
    else
      flash[:error] = flash_translate(:error)
    end

    redirect_to :action => :index
  end
end
