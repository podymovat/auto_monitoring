# encoding: UTF-8

module Web::Employee::CompaniesAccountsHelper
  def companies_people_for_accounts_collection(company_account)
    if company_account.company_id?
      company_account.company.people.asc_by_name
    elsif params[:company_account] && params[:company_account][:company_id]
      ::Company::Person.asc_by_name.find_all_by_company_id(params[:company_account][:company_id])
    else
      []
    end
  end

  def link_to_edit_account_by_employee(account)
    if account.type == ::Company::CrmAccount.name
      edit_employee_companies_crm_account_path(account)
    else account.type == ::Company::GisAccount.name
      edit_employee_companies_gis_account_path(account)
    end
  end

  def account_types_collection
    [[I18n.t('.activerecord.attributes.company/account.crm_account'), "Company::CrmAccount"], [I18n.t('.activerecord.attributes.company/account.gis_account'), "Company::GisAccount"]]
  end
end
