module EmployeeConstraintsHelper

  def employee_constraints
    session[constraints_key] || {}
  end

  def save_employee_constraints(constraints)
    session[constraints_key] = constraints
  end

  private

  def constraints_key
    "#{current_employee.class.model_name}_#{current_employee.id}_constraints"
  end
end