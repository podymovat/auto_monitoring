# encoding: UTF-8

module JsonBuilder

  class << self
    def build_search_results_for_companies(companies, view_context)
      results = []
      companies.each do |c|
        results << { value: c.to_s, href: view_context.employee_company_path(c) }
      end
      results
    end


    def build_search_results_for_company_people(company_people, view_context)
      results = []
      company_people.each do |cp|
        results << { value: "#{cp} (#{cp.company})", href: view_context.employee_company_path(cp.company) }
      end
      results
    end

    def build_search_results_for_company_invoices(company_invoices, view_context)
      results = []
      company_invoices.each do |ci|
        results << { value: "Счет №#{ci} (#{ci.company})", href: view_context.employee_company_invoice_path(ci.company, ci) }
      end
      results
    end
  end
end
