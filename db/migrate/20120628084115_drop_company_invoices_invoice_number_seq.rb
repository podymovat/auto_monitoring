class DropCompanyInvoicesInvoiceNumberSeq < ActiveRecord::Migration
  def up
    execute "DROP SEQUENCE company_invoices_invoice_number_seq"
  end

  def down
    execute "CREATE SEQUENCE company_invoices_invoice_number_seq MAXVALUE 9999 CYCLE"
  end
end
