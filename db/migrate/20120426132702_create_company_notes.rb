class CreateCompanyNotes < ActiveRecord::Migration
  def change
    create_table :company_notes do |t|
      t.text :body
      t.references :employee
      t.references :company
      t.string :state

      t.timestamps
    end
  end
end
