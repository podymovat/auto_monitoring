require 'test_helper'

class Api::EmployeesControllerTest < ActionController::TestCase

  def setup
    @signed_in_employee = create(:employee)
    sign_in(@signed_in_employee)
    @employee = create(:employee)
    @params = {:id => @employee.id, :format => :json}
  end

  test "should get show" do
    get :show, @params
    assert_response :success
  end

  test "should put update" do
    first_name = @employee.first_name

    attrs = {}
    attrs[:first_name] = generate :name
    put :update, @params.merge(:employee => attrs)
    assert_response :success

    @employee.reload
    assert @employee.first_name != first_name
  end
end
