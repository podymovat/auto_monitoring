class ScopePolicy::Policies::Company::BasePolicy < ScopePolicy::Policies::BasePolicy

  def get(id)
    object = @relation.find(id)
    unless EmployeeAccessController.can_access_company_dependent_objects?(@current_user, object)
      raise Employee::Privilege::ObjectAccessDeniedError.new("Access denied for #{object.class.model_name}", object)
    end
    object
  end
end
