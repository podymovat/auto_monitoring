class Company::InvoiceLicenseFeeDecorator < Company::InvoiceDecorator
  decorates 'company/invoice_license_fee'

  def period
    l(period_start_date) + " - " + l(period_end_date)
  end
end
