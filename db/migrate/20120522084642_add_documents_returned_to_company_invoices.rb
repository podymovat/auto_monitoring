class AddDocumentsReturnedToCompanyInvoices < ActiveRecord::Migration
  def change
    add_column :company_invoices, :documents_returned, :boolean
  end
end
