class EmployeeCredential < ActiveRecord::Base
  attr_accessible :employee_id, :privilege_id

  belongs_to :employee
  belongs_to :privilege, :class_name => ::Employee::Privilege
end
