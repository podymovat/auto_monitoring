class Employee::Privilege::ObjectAccessDeniedError < StandardError
  attr_reader :record
  def initialize(message_error, record)
    @record = record
    super(message_error)
  end
end
