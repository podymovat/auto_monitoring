class Web::Admin::EmployeesPositionsController < Web::Admin::ProtectedApplicationController
  add_i18n_breadcrumb :index, :admin_employees_positions_path

  def index
    title t(:employees_positions)

    params[:search] = {:meta_sort => 'id.asc'}.merge(params[:search] || {})
    @search = ::Employee::Position.metasearch(params[:search])
    @employees_positions = @search.page(params[:page]).per(params[:per_page])
  end

  def new
    @employee_position = ::Employee::Position.new

    add_i18n_breadcrumb :new, :new_admin_employees_position_path
  end

  def edit
    @employee_position = ::Employee::Position.find(params[:id])

    add_breadcrumb @employee_position, :edit_admin_employees_position_path
  end

  def create
    @employee_position = ::Employee::Position.new(params[:employee_position])

    if @employee_position.save
      flash[:success] = flash_translate(:success)
      redirect_to :action => :index
    else
      add_i18n_breadcrumb :new, :new_admin_employees_position_path
      render :action => :new
    end
  end

  def update
    @employee_position = ::Employee::Position.find(params[:id])

    add_breadcrumb @employee_position, :edit_admin_employees_position_path

    if @employee_position.update_attributes(params[:employee_position])
      flash[:success] = flash_translate(:success)
      redirect_to :action => :index
    else
      render :action => :edit
    end
  end

  def destroy
    @employee_position = ::Employee::Position.find(params[:id])
    @employee_position.destroy

    redirect_to :action => :index
  end
end
