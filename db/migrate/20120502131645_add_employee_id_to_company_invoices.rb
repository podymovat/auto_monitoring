class AddEmployeeIdToCompanyInvoices < ActiveRecord::Migration
  def change
    add_column :company_invoices, :employee_id, :integer
    add_index :company_invoices, :employee_id
  end
end
