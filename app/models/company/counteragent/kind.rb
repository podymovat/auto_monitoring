class Company::Counteragent::Kind < ActiveRecord::Base
  include UsefullScopes

  attr_accessible :name, :abbreviation

  validates :name, :presence => true, :uniqueness => {:case_sensitive => false},
            :length => {:maximum => 255}
  validates :abbreviation, :presence => true, :length => {:maximum => 255}

  def to_s
    name
  end
end
