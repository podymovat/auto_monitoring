$(document).ready ->
  calculate = ->
    amount = 0
    equity_amount = 0
    $(".new_invoice_product_item:visible").each ->
      if $(this).children("select").val()
        prime_cost = parseInt($(this).children(".input-prime-amount").val())
        sail_cost = parseInt($(this).children(".input-sail-amount").val())
        count = parseInt($(this).children(".input-items-count").val())
        amount += sail_cost * count
        equity_amount += (sail_cost - prime_cost) * count

    $("#company_invoice_amount").val amount
    $("#company_invoice_equity_amount").val equity_amount

  $("form").on "change", ".new_invoice_product_item > select", ->
    obj = $(this).parent()
    unless $(this).val()
      obj.children("input[type=\"text\"]").val ""
      calculate()
    $.ajax(url: Routes.api_product_item_path($(this).val())).done (data) ->
      obj.children(".input-prime-amount").val data.prime_amount_in_kop / 100
      obj.children(".input-sail-amount").val data.sail_amount_in_kop / 100
      obj.children(".input-items-count").val "1"
      calculate()

  $("form").on "change", ".new_invoice_product_item > input", ->
    calculate()

  $("form").on "click", ".new_invoice_product_item .close", (event) ->
    event.preventDefault()
    $(this).parent().hide()
    calculate()
