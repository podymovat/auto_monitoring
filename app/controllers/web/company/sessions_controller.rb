class Web::Company::SessionsController < Web::Company::ApplicationController
  def new
  end

  def create
    company_crm_account = ::Company::CrmAccount.find_by_login(params[:company_crm_account][:login])

    if company_crm_account && company_crm_account.authenticate(params[:company_crm_account][:password])
      reset_session
      sign_in(company_crm_account)
      redirect_to :company_root
    else
      flash[:error] = t('authorization.errors.incorrect_login_or_password')
      render :action => :new
    end
  end

  def destroy
    company_account_sign_out
    redirect_to :company_root
  end
end
