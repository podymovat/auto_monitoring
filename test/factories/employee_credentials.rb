FactoryGirl.define do
  factory :employee_credential do
    employee
    association :privilege, :factory => 'employee/privilege'
  end
end
