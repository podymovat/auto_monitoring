class CreateTariffs < ActiveRecord::Migration
  def change
    create_table :tariffs do |t|
      t.string :name
      t.integer :cost_in_kop, :default => 0, :null => false
      t.string :currency
      t.string :state

      t.timestamps
    end
  end
end
