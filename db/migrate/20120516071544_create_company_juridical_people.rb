class CreateCompanyJuridicalPeople < ActiveRecord::Migration
  def change
    create_table :company_juridical_people do |t|
      t.integer :company_id
      t.string :name
    end

    add_index :company_juridical_people, :company_id
  end
end
