class AddStateToCompanyInvoices < ActiveRecord::Migration
  def change
    add_column :company_invoices, :state, :string
  end
end
