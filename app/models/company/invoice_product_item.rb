class Company::InvoiceProductItem < ActiveRecord::Base
  attr_accessible :invoice_id, :product_item_id, :product_items_count, :prime_amount, :sail_amount

  belongs_to :invoice
  belongs_to :product_item, :class_name => 'ProductItem'

  composed_of :prime_amount, ComposeOfParams.money(:prime_amount_in_kop)
  composed_of :sail_amount, ComposeOfParams.money(:sail_amount_in_kop)

  validates :product_items_count,
            :numericality => { :only_integer => true, :greater_than_or_equal_to => 0 },
            :allow_nil => true
  validates :prime_amount, :presence => true, :numericality => { :greater_than_or_equal_to => 0  }
  validates :sail_amount, :presence => true, :numericality => { :greater_than_or_equal_to => 0  }

  state_machine :state, :initial => :active do
    state :active
  end
end
