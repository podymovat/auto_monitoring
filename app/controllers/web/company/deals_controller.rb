class Web::Company::DealsController < Web::Company::ProtectedApplicationController
  add_i18n_breadcrumb :general, :company_deals_path

  def index
    title t(:companies_deals)

    params[:search] = {:meta_sort => 'id.asc'}.merge(params[:search] || {})
    @search = current_company.deals.metasearch(params[:search])
    @deals = Company::DealDecorator.decorate(@search.page(params[:page]).per(params[:per_page]))
  end

  def show
    @deal = current_company.deals.find(params[:id]).decorate
    @invoices = Company::InvoiceDecorator.decorate(@deal.invoices)

    add_breadcrumb @deal.deal_number, company_deal_path(@deal)
  end
end
