FactoryGirl.define do
  factory :juridical_person do
    name
    invoice_number_prefix { generate(:name) }
  end
end
