class AddEmployeeToCompanyPeople < ActiveRecord::Migration
  def change
    change_table :company_people do |t|
      t.references :employee
    end
  end
end
