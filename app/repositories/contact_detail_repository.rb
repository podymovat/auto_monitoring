module ContactDetailRepository
  extend ActiveSupport::Concern
  include UsefullScopes

  included do
    scope :phones, where(:kind_id => ContactDetail::Kind.phone)
    scope :emails, where(:kind_id => ContactDetail::Kind.email)
  end
end
