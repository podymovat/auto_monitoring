require 'test_helper'

class Web::Admin::CompaniesStatusesControllerTest < ActionController::TestCase
  def setup
    @signed_in_employee = create(:employee)
    sign_in(@signed_in_employee)
    @company_status = create('company/status')
    @params = {:id => @company_status.id}
  end

  test "should get index" do
    get :index
    assert_response :success
  end

  test "should get new" do
    get :new
    assert_response :success
  end

  test "should post create" do
    attrs = attributes_for 'company/status'

    post :create, :company_status => attrs
    assert_response :redirect

    status = ::Company::Status.last
    assert_equal attrs[:name], status.name
  end

  test "should get edit" do
    get :edit, @params
    assert_response :success
  end

  test "should put update" do
    attrs = {}
    attrs[:name] = generate :name
    put :update, @params.merge(:company_status => attrs)
    assert_response :redirect

    @company_status.reload
    assert_equal attrs[:name], @company_status.name
  end

  test "should delete destroy" do
    delete :destroy, @params
    assert_response :redirect

    assert !::Company::Status.exists?(@params[:id])
  end
end
