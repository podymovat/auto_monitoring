class Api::SearchController < Api::ApplicationController

  def index
    results = []
    query = params[:term] ? params[:term].strip : ''
    unless query.empty?
      companies = ::Company.ilike_by_contact_details_entry(query)
      company_people = ::Company::Person.ilike_by_contact_details_entry(query)
      company_invoices = ::Company::Invoice.ilike_by_invoice_number(query)

      results += JsonBuilder.build_search_results_for_companies(companies, view_context)
      results += JsonBuilder.build_search_results_for_company_people(company_people, view_context)
      results += JsonBuilder.build_search_results_for_company_invoices(company_invoices, view_context)
    end

    respond_with(results.to_json)
  end
end
