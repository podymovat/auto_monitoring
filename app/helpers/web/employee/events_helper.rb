module Web::Employee::EventsHelper

  def search_events
    [[t('.expired'), :expired], [t('.today'), :today], [t('.on_this_week'), :this_week]]
  end

  def search_selected args
    return unless args.kind_of?(Hash)
    args[:employee_event_search]
  end

  def companies_people_for_events_collection(event)
    if event.company_id?
      event.company.people.asc_by_name
    elsif params[:employee_event] && params[:employee_event][:company_id]
      ::Company::Person.asc_by_name.find_all_by_company_id(params[:employee_event][:company_id])
    else
      []
    end
  end

  def event_due_date_label(event)
    if Employee::Event.expired.include? event
      status = :expired
    elsif Employee::Event.today.include? event
      status = :today
    else
      status = :upcoming
    end

    cls = "label label-event-due-date-#{status}"
    content_tag(:span, l(event.due_date), { :class => cls })
  end
end
