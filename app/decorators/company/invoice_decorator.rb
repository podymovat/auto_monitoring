class Company::InvoiceDecorator < ApplicationDecorator
  decorates 'company/invoice'

  def amount
    source.amount.format
  end

  def equity_amount
    source.equity_amount.format
  end
end
