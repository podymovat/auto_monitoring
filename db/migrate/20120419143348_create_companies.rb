class CreateCompanies < ActiveRecord::Migration
  def up
    create_table :companies do |t|
      t.string :name
      t.references :employee
      t.string :state
      t.string :payment_method

      t.timestamps
    end
    add_index :companies, :employee_id
  end

  def down
    drop_table :companies
  end
end
