class Web::Company::GisAccountsController < Web::Company::ProtectedApplicationController
  add_i18n_breadcrumb :general, :company_accounts_path
  def edit
    @gis_account = current_company.gis_accounts.find(params[:id])

    add_breadcrumb @gis_account, :edit_company_gis_account_path
  end

  def update
    @gis_account = current_company.gis_accounts.find(params[:id])

    if @gis_account.update_attributes(params[:company_gis_account])
      flash[:success] = flash_translate(:success)
      redirect_to company_accounts_path
    else
      add_breadcrumb @gis_account.login, :edit_company_gis_account_path
      render :action => :edit
    end
  end
end
