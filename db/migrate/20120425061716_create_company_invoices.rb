class CreateCompanyInvoices < ActiveRecord::Migration
  def change
    create_table :company_invoices do |t|
      t.integer :company_id
      t.integer :kind_id
      t.integer :amount_in_kop
      t.string :currency
      t.string :payment_state

      t.timestamps
    end
  end
end
