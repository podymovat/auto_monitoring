class ProductItem < ActiveRecord::Base
  attr_accessible :name, :prime_amount, :sail_amount

  belongs_to :creator, :class_name => ::Employee, :foreign_key => 'employee_id'

  composed_of :prime_amount, ComposeOfParams.money(:prime_amount_in_kop)
  composed_of :sail_amount, ComposeOfParams.money(:sail_amount_in_kop)

  has_many :invoice_product_items
  has_many :invoices, :through => :invoice_product_items

  validates :name, :presence => true, :uniqueness => {:case_sensitive => false},
            :length => {:maximum => 255}
  validates :prime_amount, :presence => true, :numericality => { :greater_than_or_equal_to => 0  }
  validates :sail_amount, :presence => true, :numericality => { :greater_than_or_equal_to => 0  }

  state_machine :state, :initial => :active do
    state :active
  end

  def to_s
    name
  end
end
