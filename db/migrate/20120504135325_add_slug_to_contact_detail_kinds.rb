class AddSlugToContactDetailKinds < ActiveRecord::Migration
  def change
    add_column :contact_detail_kinds, :slug, :string
  end
end
