class AddExpectedPaymentDateToCompanyInvoices < ActiveRecord::Migration
  def change
    add_column :company_invoices, :expected_payment_date, :date
  end
end
