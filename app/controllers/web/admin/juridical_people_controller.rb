class Web::Admin::JuridicalPeopleController < Web::Admin::ProtectedApplicationController
  add_i18n_breadcrumb :index, :admin_juridical_people_path

  def index
    title t(:juridical_people)

    params[:search] = {:meta_sort => 'created_at.asc'}.merge(params[:search] || {})
    @search = ::JuridicalPerson.metasearch(params[:search])
    @juridical_people = @search.page(params[:page]).per(params[:per_page])
  end

  def new
    @juridical_person = ::JuridicalPerson.new

    add_i18n_breadcrumb :new, :new_admin_juridical_person_path
  end

  def edit
    @juridical_person = ::JuridicalPerson.find(params[:id])

    add_breadcrumb @juridical_person, :edit_admin_juridical_person_path
  end

  def create
    @juridical_person = ::JuridicalPerson.new(params[:juridical_person])

    if @juridical_person.save
      flash[:success] = flash_translate(:success)
      redirect_to :action => :index
    else
      add_i18n_breadcrumb :new, :new_admin_juridical_person_path
      render :action => :new
    end
  end

  def update
    @juridical_person = ::JuridicalPerson.find(params[:id])

    add_breadcrumb @juridical_person, :edit_admin_juridical_person_path

    if @juridical_person.update_attributes(params[:juridical_person])
      flash[:success] = flash_translate(:success)
      redirect_to :action => :index
    else
      render :action => :edit
    end
  end

  def destroy
    @juridical_person = ::JuridicalPerson.find(params[:id])
    @juridical_person.destroy

    redirect_to :action => :index
  end
end
