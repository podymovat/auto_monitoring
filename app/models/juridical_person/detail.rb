class JuridicalPerson::Detail < ActiveRecord::Base
  include UsefullScopes

  attr_accessible :juridical_person_id, :kind_id, :value

  belongs_to :kind
  belongs_to :juridical_person

  validates :value, :presence => true, :length => {:maximum => 256}
  validates :kind, :presence => true

  state_machine :state, :initial => :active do
    state :active
  end

  def to_s
    value
  end
end
