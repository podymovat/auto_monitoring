class Web::Employee::CompaniesPeopleController < Web::Employee::ProtectedApplicationController
  add_i18n_breadcrumb :general, :employee_companies_path

  def index
    title t(:companies_people)

    params[:search] = {:meta_sort => 'id.asc'}.merge(params[:search] || {})
    scope = build_scope 'company/person'
    @search = scope.people_list.metasearch(params[:search])
    @companies_people = @search.page(params[:page]).per(params[:per_page])
  end

  def new
    @company_person = ::Company::Person.new(params[:company_person])
    @company_person.contact_details.build({ :kind_id => ContactDetail::Kind.phone.id })

    add_i18n_breadcrumb :new, :new_employee_companies_person_path
  end

  def create
    @company_person = ::Company::Person.new(params[:company_person])
    @company_person.creator = current_employee

    if @company_person.save
      flash[:success] = flash_translate(:success)
      redirect_to employee_company_path(@company_person.company)
    else
      add_i18n_breadcrumb :new, :new_employee_companies_person_path
      render :action => :new
    end
  end

  def edit
    scope = build_scope 'company/person'
    @company_person = scope.get(params[:id])

    add_breadcrumb @company_person.company, employee_company_path(@company_person.company)
    add_breadcrumb @company_person, :edit_employee_companies_person_path
  end

  def update
    scope = build_scope 'company/person'
    @company_person = scope.get(params[:id])

    if @company_person.update_attributes(params[:company_person])
      flash[:success] = flash_translate(:success)
      redirect_to employee_company_path(@company_person.company)
    else
      add_breadcrumb @company_person.company, employee_company_path(@company_person.company)
      add_breadcrumb @company_person.name, :edit_employee_companies_person_path
      render :action => :edit
    end
  end

  def destroy
    scope = build_scope 'company/person'
    @company_person = scope.get(params[:id])
    @company_person.destroy

    redirect_to employee_company_path(@company_person.company)
  end
end
