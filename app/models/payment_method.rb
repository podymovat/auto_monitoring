class PaymentMethod < ActiveRecord::Base
  attr_accessible :name

  validates :name, :presence => true, :uniqueness => {:case_sensitive => false}

  def to_s
    name
  end

  class << self

    def cash
      find(1)
    end
  end
end
