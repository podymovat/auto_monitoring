class Web::Employee::TariffsController < Web::Employee::ProtectedApplicationController
  def index
    title t(:tariffs)
    @tariffs = TariffDecorator.decorate(Tariff.page(params[:page]).per(params[:per_page]))
  end

  def new
    @tariff = Tariff.new
  end

  def create
    @tariff = Tariff.new(params[:tariff])

    if @tariff.save
      flash[:success] = flash_translate(:success)
      redirect_to :action => :index
    else
      render :action => :new
    end
  end

  def edit
    @tariff = Tariff.find(params[:id])
  end

  def update
    @tariff = Tariff.find(params[:id])

    if @tariff.update_attributes(params[:tariff])
      flash[:success] = flash_translate(:success)
      redirect_to :action => :index
    else
      render :action => :new
    end
  end

  def destroy
    @tariff = Tariff.find(params[:id])
    @tariff.destroy
    redirect_to :action => :index
  end
end
