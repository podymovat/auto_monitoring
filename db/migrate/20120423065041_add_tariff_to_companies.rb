class AddTariffToCompanies < ActiveRecord::Migration
  def change
    change_table :companies do |t|
      t.references :tariff
    end
  end
end
