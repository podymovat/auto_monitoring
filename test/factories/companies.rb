FactoryGirl.define do
  factory :company do
    name
    creator
    assigned
    tariff
    payment_method
    rental_fee
    association :category, :factory => 'company/category'
    association :status, :factory => 'company/status'

    after(:create) do |c|
      FactoryGirl.create('company/note', company: c)
      FactoryGirl.create('company/person', company: c)
      counteragent = FactoryGirl.create('company/counteragent', company: c)
      d = FactoryGirl.create('company/deal', company: c, counteragent: counteragent)
      FactoryGirl.create('company/invoice', company: c, deal: d)
      p = FactoryGirl.create('company/person', company: c)
      FactoryGirl.create('company/crm_account', company: c, person: p)
      FactoryGirl.create('company/gis_account', company: c)
    end

    before(:create) do |c|
      # make sure that you run db:seed before running tests
      # build 1 phone for the company
      kind_phone = ContactDetail::Kind.phone
      kind_phone_value = FactoryGirl.generate(:phone)
      c.contact_details.build(:kind_id => kind_phone.id, :kind_value_source => kind_phone_value)

      kind_email = ContactDetail::Kind.email
      kind_email_value = FactoryGirl.generate(:email)
      c.contact_details.build(:kind_id => kind_email.id, :kind_value_source => kind_email_value)
    end
  end
end
