class JuridicalPerson < ActiveRecord::Base
  include UsefullScopes

  attr_accessible :invoice_number_prefix, :name, :details_attributes

  has_many :details, :dependent => :destroy

  validates :name, :presence => true, :length => {:maximum => 255}
  validates :invoice_number_prefix, :presence => true, :length => {:maximum => 255}

  accepts_nested_attributes_for :details, :allow_destroy => true, :reject_if => :all_blank

  state_machine :state, :initial => :active do
    state :active
  end

  def next_invoice_number
    increment(:invoice_number_counter)
    invoice_number_counter
  end

  def to_s
    name
  end
end
