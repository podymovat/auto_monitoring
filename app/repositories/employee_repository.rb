module EmployeeRepository
  extend ActiveSupport::Concern

  included do
    scope :asc_by_full_name, order("employees.first_name ASC, employees.last_name ASC")
    scope :active, where(:state => :active)
  end
end
