class RemoveSlugFromCompanyStatuses < ActiveRecord::Migration
  def up
    remove_column :company_statuses, :slug
  end

  def down
    add_column :company_statuses, :slug, :string
  end
end
