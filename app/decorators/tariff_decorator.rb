class TariffDecorator < ApplicationDecorator
  decorates :tariff

  def cost
    source.cost.format
  end
end
