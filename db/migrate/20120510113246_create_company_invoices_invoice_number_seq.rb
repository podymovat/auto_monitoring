class CreateCompanyInvoicesInvoiceNumberSeq < ActiveRecord::Migration
  def up
    execute "CREATE SEQUENCE company_invoices_invoice_number_seq MAXVALUE 9999 CYCLE"
  end

  def down
    execute "DROP SEQUENCE company_invoices_invoice_number_seq"
  end
end
