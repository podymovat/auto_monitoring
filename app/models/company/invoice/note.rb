class Company::Invoice::Note < ActiveRecord::Base
  attr_accessible :body, :invoice_id

  belongs_to :invoice
  belongs_to :creator, :class_name => ::Employee, :foreign_key => "employee_id"

  validates :invoice, :presence => true
  validates :creator, :presence => true
  validates :body, :presence => true

  state_machine :state, :initial => :published do
    state :published
  end

  def short_body
    body.truncate(100)
  end
end
