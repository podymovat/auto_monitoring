class Web::Employee::ApplicationController < Web::ApplicationController
  layout 'web/employee/application'

  before_filter :employee_title
  helper_method :employee_signed_in?, :current_employee, :permission,
                :employee_constraints

  private

  def employee_title
    title t("title.employee")
  end

  rescue_from ::Employee::Privilege::ObjectAccessDeniedError do |e|
    flash[:error] = flash_translate(:error, :assigned => e.record.is_a?(::Company) ? e.record.assigned : e.record.company.assigned)
    redirect_to employee_root_path
  end

  rescue_from ::Employee::Privilege::AccessDeniedError do
    flash[:error] = flash_translate(:error)
    redirect_to employee_root_path
  end
end
