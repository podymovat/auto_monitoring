require 'test_helper'

class Web::Employee::Companies::PeopleControllerTest < ActionController::TestCase
  def setup
    @employee = create(:employee)
    sign_in(@employee)
    @company = create(:company, :creator => @employee, :assigned => @employee)
    @company_person = @company.people.last
    @params = {:company_id => @company.id, :id => @company_person.id}
  end

  test "should get show" do
    get :show, @params
    assert_response :success
  end
end
