class Company::CrmAccount < Company::Account
  has_secure_password
  attr_accessible :person_id, :password

  belongs_to :person

  validates :person, :presence => true
end
