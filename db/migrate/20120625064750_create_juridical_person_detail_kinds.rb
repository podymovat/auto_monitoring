class CreateJuridicalPersonDetailKinds < ActiveRecord::Migration
  def change
    create_table :juridical_person_detail_kinds do |t|
      t.string :name
    end

    add_index :juridical_person_detail_kinds, :name, :unique => true
  end
end
