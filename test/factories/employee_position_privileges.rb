FactoryGirl.define do
  factory :employee_position_privilege do
    association :position, :factory => 'employee/position'
    association :privilege, :factory => 'employee/privilege'
  end
end
