FactoryGirl.define do
  factory 'company/status', :aliases => [:company_status] do
    name
    color
  end
end
