# coding: UTF-8

module AuthHelper
  def sign_in(user)
    session[user.class.model_name] = user.id
  end

  def employee_sign_out
    session[::Employee.model_name] = nil
  end

  def employee_signed_in?
    session[::Employee.model_name]
  end

  def authenticate_employee!
    unless employee_signed_in?
      redirect_to new_employee_session_path
    end
  end

  def current_employee
    @current_employee ||= session[::Employee.model_name] && ::Employee.find(session[::Employee.model_name])
  end

  def authenticate_admin!
    unless employee_signed_in?
      redirect_to new_employee_session_path
    end
  end

  def company_account_sign_out
    session[::Company::CrmAccount.model_name] = nil
  end

  def company_account_signed_in?
    session[::Company::CrmAccount.model_name]
  end

  def authenticate_company_account!
    unless company_account_signed_in?
      redirect_to new_company_session_path
    end
  end

  def current_company_account
    @current_company_account ||= session[::Company::CrmAccount.model_name] &&
      ::Company::CrmAccount.find(session[::Company::CrmAccount.model_name])
  end

  def basic_auth
    authenticate_or_request_with_http_basic do |user, password|
      user == configus.basic_auth.username && password == configus.basic_auth.password
    end
  end
end
