module Employee::PositionRepository
  extend ActiveSupport::Concern

  included do
    scope :with_department, lambda { |d| where(:department_id => d)}
    scope :without_department, where(:department_id => nil)
  end
end
