class Api::EmployeesController < Api::ApplicationController
  def update
    @employee = ::Employee.find(params[:id])
    @employee.update_attributes!(params[:employee])
    respond_with(@employee)
  end

  def show
    @employee = ::Employee.find(params[:id])
    respond_with(@employee)
  end
end

