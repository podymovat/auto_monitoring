class NewCompanyGisAccountType < Company::GisAccount
  include BaseType

  before_save do
    self.type = Company::GisAccount.name
  end
end
